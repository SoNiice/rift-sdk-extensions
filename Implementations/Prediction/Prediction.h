#pragma once

#include "SDK Extensions.h"

class Prediction : public IPrediction {
public:
	static struct DefaultValues_t {
		bool  CheckAAWindup  = true;
		float MaxRangeIgnore = 50.0f;
		float ReactionDelay  = 0.0f;
		float SpellDelay     = 0.0f;
	} DefaultValues;
		
	struct Output : public IPrediction::Output {
		Output(std::shared_ptr<IPrediction::Input> inp, AIBaseClient* unit) {
			Input = inp;
			Unit = unit;			
		}
		Output(std::shared_ptr<IPrediction::Input> inp, AIBaseClient* unit, Vector2 castpos, Vector2 unitpos, HitChance hc, std::shared_ptr<ICollision::Output> col) : Output(inp, unit) {
			CastPosition = castpos;
			UnitPosition = unitpos;
			Hitchance = hc;
			CollisionResult = col;
		}		

		void Lock(bool checkDodge = true);		
		void CheckOutofRange(bool checkDodge);
		void CheckCollisions();
	};

	Prediction();
	~Prediction() {};

	Vector2 GetFastPrediction(AIBaseClient * target, float delay, float missileSpeed = HUGE_VALF, Vector2* from = NULL, float distanceSet = 0);
	
	std::shared_ptr<IPrediction::Output> GetPrediction(AIHeroClient* target, std::shared_ptr <IPrediction::Input> Data);
	std::shared_ptr<IPrediction::Output> GetPrediction(
		AIHeroClient* target,
		float width = 100.0f,
		float delay = 0.25f,
		float missileSpeed = HUGE_VALF,
		float range = HUGE_VALF,
		SkillshotType type = SkillshotType::Line,
		bool collisionable = false,
		CollisionFlags Flags = CollisionFlags::Default,
		Vector3 From = Player.GetPosition(),
		Vector3 RangeCheckFrom = Player.GetPosition()
	);
	
	static std::shared_ptr<Output> WaypointAnalysis(AIHeroClient* target, std::shared_ptr<IPrediction::Input> input, std::vector<Vector2>& path, Vector2& from);

	static Vector3 PositionAfter(AIBaseClient * Unit, float t, float speed);
	static Vector3 PositionAfter(std::vector<Vector3>& Path, float t, float speed);
	static Vector3 PositionAfter(std::vector<Vector2>& Path, float t, float speed);

	static Vector3 GetFrom(std::shared_ptr<IPrediction::Input> input);
	static Vector3 GetRangeCheckFrom(std::shared_ptr<IPrediction::Input> input);

	static float   GetArrivalTime(float distance, float delay, float missileSpeed = 0);
	static HitChance GetHitChance(AIHeroClient* Unit, float t);
};

