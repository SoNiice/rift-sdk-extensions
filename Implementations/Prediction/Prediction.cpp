#include "Prediction.h"
#include "PathTracker.h"
#include "Geometry.hpp"

Prediction::DefaultValues_t Prediction::DefaultValues;

Prediction::Prediction() {
	PathTracker::Init();	
}

//distanceSet - distance target can walk before being hit, if left 0 will get calculated
Vector2 Prediction::GetFastPrediction(AIBaseClient* target, float delay, float missileSpeed, Vector2* from, float distanceSet) {
	auto nav{ target->NavInfo() };
	if (nav.IsDashing) {
		return nav.EndPos;
	}

	auto fromPos{ Player.GetPosition().To2D() };
	auto targPos{ target->GetPosition().To2D() };
	std::vector<Vector2> path{ target->GetWaypoints2D()};
	path.insert(path.begin(), targPos);

	if (from != NULL) {		
		fromPos = *from;
	}

	if (path.size() <= 1) {
		return targPos;
	}

	float time{ 0.0f };
	float distance{ distanceSet };
	if (distance < EPSILON) {
		float targetDistance = fromPos.Distance(targPos);
		
		if (missileSpeed > EPSILON && (missileSpeed + EPSILON) < HUGE_VALF) {			
			Vector2 Vt = (path[1] - path[0]).Normalized() * target->GetMovementSpeed();
			Vector2 Vs = (targPos - fromPos).Normalized() * missileSpeed;
			Vector2 Vr = Vt - Vs;

			time = targetDistance / Vr.Length();
		}

		time += delay + (Game::Ping() / 2000.0f);
		distance = time * target->GetMovementSpeed();
	}

	if (target->IsImmobile(time)) {
		return targPos;
	}

	for (size_t i = 1; i < path.size(); ++i) {
		float d = path[i].Distance(path[i - 1]);
		if (distance == d)
			return path[i];
		else if (distance < d)
			return path[i - 1].Extended(path[i], distance);
		else distance -= d;
	}

	return path.back();
}

std::shared_ptr<IPrediction::Output> Prediction::GetPrediction(AIHeroClient* target, std::shared_ptr<IPrediction::Input> input) {
	std::shared_ptr<Prediction::Output> result{ std::make_shared<Prediction::Output>(input, target) };	
	
	auto &EnemyInfo{ PathTracker::GetData(target) };
	float moveSpeed{ target->GetMovementSpeed() };
	auto  targetPos{ target->GetPosition() };
	auto  serverPos{ target->GetServerPosition() };

	//Dummy Targets
	if (moveSpeed < 1.0f) {
		result->Hitchance = HitChance::Immobile;
		result->CastPosition = targetPos;
		result->UnitPosition = targetPos;

		return result;
	}

	auto from{ GetFrom(input) };
	auto rangeCheckFrom{ GetRangeCheckFrom(input) };
	auto bR{ target->GetBoundingRadius() };
	auto hitboxExtraDistance{ (input->Width*0.5f + bR) };
	auto timeSinceLastAA{ GetTickCount() - EnemyInfo.LastAATick };
	auto timeToReach {GetArrivalTime(targetPos.Distance(from), input->Delay, input->MissileSpeed)};
	timeToReach -= hitboxExtraDistance / moveSpeed;	

	auto from2D{ from.To2D() };
	auto currentPath{ target->GetWaypoints2D() };
	currentPath.insert(currentPath.begin(), targetPos.To2D());

	//Target Immobile
	float ImmobileTimeLeft{ target->ImmobileTimeLeft() };
	if (ImmobileTimeLeft > 0.0f) {
		result->CastPosition = serverPos;
		result->UnitPosition = serverPos;
		
		if (timeToReach <= (ImmobileTimeLeft + EnemyInfo.AvgReactionTime())) {
			result->Hitchance = HitChance::Immobile;
			result->Lock(false);		

			return result;
		}

		result->Hitchance = HitChance::VeryHigh;
		result->Lock(false);
		return result;
	}
	
	//Target Channeling Interruptible Spell
	if (target->IsChannelingImportantSpell()) {
		result->Hitchance = target->IsChannelingImportantSpell(timeToReach, true) ? HitChance::Immobile : HitChance::VeryHigh;
		result->CastPosition = serverPos;
		result->UnitPosition = serverPos;
		result->Lock(false);

		return result;
	}

	//Target Dashing/Blinking
	auto dashData{ pSDK->GapcloserManager->GetGapcloseDataInst(target->GetNetworkID()) };
	auto navInfo{ target->NavInfo() };
	if (dashData.Data.IsValid()) {	
		auto curPos{ targetPos.To2D() };
		auto endPos{ dashData.Spell.EndPosition.To2D() };
		float enemyHeight{ targetPos.y - targetPos.GetTerrainHeight() };	

		//Impact During Dash:
		if (!dashData.Data.IsBlink) {
			auto timeSinceDashStarted{ Game::Time() - dashData.Spell.StartTime };
			auto hitboxExtraTime{ hitboxExtraDistance * 0.8f / dashData.Data.Speed };
			auto dashDelay{ std::max<float>(0.0f, dashData.Data.WindUp - timeSinceDashStarted) };
			auto [time, impactPos] { VectorMovementCollisionEx(curPos, endPos, dashData.Data.Speed, from2D, input->MissileSpeed, input->Delay + (Game::Ping() / 2000.0f) - dashDelay - hitboxExtraTime) };
						
			if (impactPos.IsValid() && impactPos.Distance(curPos, endPos, true) < 200.0f) {
				result->CastPosition = impactPos.To3D(impactPos.GetTerrainHeight() + enemyHeight);
				result->UnitPosition = result->CastPosition;
				result->Hitchance = HitChance::Dashing;

				result->Lock(false);
				return result;
			}
		}		

		auto hitboxTimeWindow { (hitboxExtraDistance*0.8f) / moveSpeed };
		auto timeUntilDashEnds{ dashData.EndTime - Game::Time() };
		auto timeToReachEnd   { GetArrivalTime(endPos.Distance(from), input->Delay, input->MissileSpeed)};		

		//Impact Right At The End Of Dash/Blink:
		if ((timeToReachEnd - timeUntilDashEnds) >= 0.0f && (timeToReachEnd - timeUntilDashEnds) < hitboxTimeWindow) {
			result->CastPosition = endPos.To3D(endPos.GetTerrainHeight() + enemyHeight);
			result->UnitPosition = result->CastPosition;
			result->Hitchance = HitChance::Dashing;				

			result->Lock(false);
			return result;			
		}		
	}		
		
	//Target Winding Up
	auto timeToCompleteAA{ std::max<float>(target->GetAttackCastDelay() + 0.025f + EnemyInfo.AvgMovChangeTime(), EnemyInfo.AvgOrbwalkTime) };
	if (timeSinceLastAA < timeToCompleteAA && DefaultValues.CheckAAWindup) {
		auto timeStandingStill{ timeToCompleteAA - (timeSinceLastAA/1000.0f) };
		if (timeStandingStill >= timeToReach) {
			result->Hitchance = HitChance::VeryHigh;
			result->CastPosition = targetPos;
			result->UnitPosition = targetPos;
			result->Lock();			

			return result;
		}
	}

	//Target Running Straight Away or Into Player
	if (currentPath.size() >= 2) {
		auto movementVec{ currentPath.back() - targetPos };
		auto relativeVec{ from - targetPos };
		auto angle{ movementVec.AngleBetween(relativeVec) };

		if (angle > 160.0f || angle < 20.0f || (angle > 145.0f && movementVec.Length() > 400.0f)) {
			result->CastPosition = GetFastPrediction(target, timeToReach, HUGE_VALF, &from2D);
			result->UnitPosition = result->CastPosition;
			result->Hitchance = HitChance::VeryHigh;
			result->Lock();

			return result;
		}
	}

	//Target Changing Direction
	if (EnemyInfo.AvgPathLenght < 400.0f && EnemyInfo.LastMovChangeTime() < 100 && EnemyInfo.PathLength <= EnemyInfo.AvgPathLenght) {
		auto posOfCollision{ PositionAfter(currentPath, timeToReach, target->GetMovementSpeed()) };
		if (posOfCollision.IsValid()) {
			result->Hitchance = HitChance::High;
			result->CastPosition = posOfCollision;
			result->UnitPosition = posOfCollision;
			result->Lock();

			return result;
		}
	}

	//Standard Generic Pred	
	result = WaypointAnalysis(target, input, currentPath, from2D);

	//Reduced Hitchance if Enemy Usually Changes Direction
	float timeToReact{ result->CastPosition.Distance(targetPos)/ moveSpeed };
	float timeUntilChangesDirection{ EnemyInfo.AvgReactionTime() - (EnemyInfo.LastMovChangeTime()/1000.0f) };
	if (timeToReact >= timeUntilChangesDirection)
		result->Hitchance = HitChance::Medium;	

	//Target Not Moving and Not Attacking
	bool MovingTarget{ target->IsMoving() };
	if (!MovingTarget && EnemyInfo.LastMovChangeTime() > 300 && (timeSinceLastAA > 300 || !DefaultValues.CheckAAWindup))
		result->Hitchance = HitChance::High; 	
	
	//Target Hasnt Changed Direction In A While
	if (EnemyInfo.LastMovChangeTime() > 250 && timeToReach < 0.5f) 
		result->Hitchance = HitChance::High;
	
	result->Lock();
	return result;	
}
std::shared_ptr<IPrediction::Output> Prediction::GetPrediction(AIHeroClient* target, float width, float delay, float missileSpeed, float range, SkillshotType type, bool collisionable, CollisionFlags flags, Vector3 from, Vector3 rangeCheckFrom) {
	std::shared_ptr<IPrediction::Input> input{ std::make_shared<IPrediction::Input>(type, range, delay, missileSpeed, width, collisionable, flags, from) };
	input->RangeCheckFrom = rangeCheckFrom;
	return GetPrediction(target, input);
}

#pragma region Internal Methods

std::shared_ptr<Prediction::Output> Prediction::WaypointAnalysis(AIHeroClient * target, std::shared_ptr<IPrediction::Input> input, std::vector<Vector2>& path, Vector2 & from) {
	auto moveSpeed{ target->GetMovementSpeed() };
	if (moveSpeed < EPSILON) { moveSpeed = 350.0f; } //This is only in case the Sdk method fails

	auto result {std::make_shared<Prediction::Output>(input, target)};
	
	float extraSpellDelay{ DefaultValues.SpellDelay};
	float extraPing{ Game::Ping() / 2000.f };
	float flyTimeMax = input->MissileSpeed != 0 ? input->Range / input->MissileSpeed : 0.f;

	float tMin = input->Delay + extraPing + extraSpellDelay;
	float tMax = flyTimeMax + input->Delay + 2.0f*extraPing + extraSpellDelay;
	float pathTime = 0.f;
	int pathBounds[]{ -1, -1 };

	//find bounds
	for (size_t i = 1; i < path.size(); i++) {
		float t = path[i].Distance(path[i - 1]) / moveSpeed;

		if (pathTime <= tMin && pathTime + t >= tMin)
			pathBounds[0] = (int)i - 1;
		if (pathTime <= tMax && pathTime + t >= tMax)
			pathBounds[1] = (int)i - 1;

		if (pathBounds[0] != -1 && pathBounds[1] != -1)
			break;

		pathTime += t;
	}

	//calculate cast & unit position
	if (pathBounds[0] != -1 && pathBounds[1] != -1) {
		auto width{ std::max<float>(input->Width, 30.0f) };
		for (size_t k = (size_t)pathBounds[0]; k <= (size_t)pathBounds[1]; k++) {
			float extender = target->GetBoundingRadius();

			if (input->SkillType == SkillshotType::Line)
				extender += width;

			Vector2 direction = (path[k + 1] - path[k]).Normalized();			
			int steps = (int)std::floor(path[k].Distance(path[k + 1]) / width);
			//split & analyze current path
			for (int i = 1; i < (steps - 1); i++) {
				Vector2 pCenter = path[k] + (direction * width * (float)i);
				Vector2 pA = pCenter - (direction * extender);
				Vector2 pB = pCenter + (direction * extender);

				float flytime = input->MissileSpeed != 0 ? from.Distance(pCenter) / input->MissileSpeed : 0.f;
				float t = flytime + input->Delay + extraPing + extraSpellDelay;

				Vector2 currentPosition = target->GetPosition().To2D();

				float arriveTimeA = currentPosition.Distance(pA) / moveSpeed;
				float arriveTimeB = currentPosition.Distance(pB) / moveSpeed;

				if (min(arriveTimeA, arriveTimeB) <= t && max(arriveTimeA, arriveTimeB) >= t) {
					result->Hitchance = GetHitChance(target, t);
					result->CastPosition = pCenter;
					result->UnitPosition = pCenter; //+ (direction * (t - min(arriveTimeA, arriveTimeB)) * moveSpeed);
					return result;
				}
			}
		}
	}

	result->Hitchance = HitChance::Impossible;
	result->CastPosition = target->GetPosition();

	return result;
}
Vector3 Prediction::PositionAfter(AIBaseClient* Unit, float t, float speed) {
	auto distance = t * speed;
	std::vector<Vector3> Path{ Unit->GetWaypoints() };

	for (size_t i = 1; i < Path.size(); i++) {
		auto a = Path[i - 1];
		auto b = Path[i];
		auto d = a.Distance(b);

		if (d < distance) {
			distance -= d;
		}
		else {
			return a + distance * (b - a).Normalized();
		}
	}
	return Path.size() > 0 ? Path[Path.size() - 1] : Vector3();
}
Vector3 Prediction::PositionAfter(std::vector<Vector3>& Path, float t, float speed) {
	auto distance = t * speed;	

	for (size_t i = 1; i < Path.size(); i++) {
		auto a = Path[i - 1];
		auto b = Path[i];
		auto d = a.Distance(b);

		if (d < distance) {
			distance -= d;
		}
		else {
			return a + distance * (b - a).Normalized();
		}
	}
	return Path.size() > 0 ? Path[Path.size() - 1] : Vector3();
}
Vector3 Prediction::PositionAfter(std::vector<Vector2>& Path, float t, float speed) {
	auto distance = t * speed;

	for (size_t i = 1; i < Path.size(); i++) {
		auto a = Path[i - 1];
		auto b = Path[i];
		auto d = a.Distance(b);

		if (d < distance) {
			distance -= d;
		}
		else {
			auto pos{ a + distance * (b - a).Normalized() };
			return pos.To3D(pos.GetTerrainHeight());
		}
	}

	return Path.empty() ? Vector3() : Path.back().To3D(Path.back().GetTerrainHeight());
}
float   Prediction::GetArrivalTime(float distance, float delay, float missileSpeed) {
	float Time{ delay + Game::Ping() / 2000.0f };
	if (missileSpeed > EPSILON)
		Time += distance / missileSpeed;
	return Time;
}
HitChance Prediction::GetHitChance(AIHeroClient* Unit, float t) {
	auto &Data{ PathTracker::GetData(Unit) };
	if (Data.AvgPathLenght > 400.0f) {
		if (Data.LastMovChangeTime() > 50) {
			if (Data.AvgReactionTime() >= t) {
				if (Data.LastAngleDiff < 30.0f) {
					return HitChance::VeryHigh;
				}
				else {
					return HitChance::High;
				}
			}
			else if (Data.AvgReactionTime() - Data.LastMovChangeTime() >= t) {
				return HitChance::Medium;
			}

			else {
				return HitChance::Low;
			}
		}
		else {
			return HitChance::VeryHigh;
		}
	}
	else {
		return HitChance::High;
	}
}

inline Vector3 Prediction::GetFrom(std::shared_ptr<IPrediction::Input> input) {
	return input->From.IsValid() ? input->From : Player.GetPosition();
}
inline Vector3 Prediction::GetRangeCheckFrom(std::shared_ptr<IPrediction::Input> input) {
	return input->RangeCheckFrom.IsValid() ? input->RangeCheckFrom : GetFrom(input);
}
#pragma endregion


#pragma region Output Internal Methods
inline void Prediction::Output::Lock(bool checkDodge) {
	if (this->Input->Collision) {
		auto fromPos{ GetFrom(this->Input) };
		this->CollisionResult = pSDK->Collision->GetCollisions(fromPos, this->CastPosition, this->Input->Width, this->Input->Delay, this->Input->MissileSpeed, false, this->Input->Flags);
		this->CheckCollisions();
	}		
	this->CheckOutofRange(checkDodge);
}
inline void Prediction::Output::CheckCollisions() {
	if (this->CollisionResult && this->Input->Flags & this->CollisionResult->Objects)
		this->Hitchance = HitChance::Collision;
}
inline void Prediction::Output::CheckOutofRange(bool checkDodge) {
	auto rangeIgnoreMod{ (100.0f - DefaultValues.MaxRangeIgnore) / 100.f };
	auto dist{ GetRangeCheckFrom(this->Input).Distance(this->CastPosition) - (this->Input->SkillType == SkillshotType::Circle ? this->Input->Width : 0.0f) };

	if (dist > this->Input->Range - (checkDodge ? GetArrivalTime(dist, this->Input->Delay, this->Input->MissileSpeed) * this->Unit->GetMovementSpeed() * rangeIgnoreMod : 0.0f))
		this->Hitchance = HitChance::OutOfRange;
}
#pragma endregion