#pragma once

#include "SDK Extensions.h"
#include "Geometry.hpp"

class Collision : public ICollision {
	struct YasuoWallData {
		GameObject* Object;
		unsigned int CastTick;
		int Level;
		Vector3 StartPosition;
	};

	static std::vector<YasuoWallData> YasuoWalls;

	static void __cdecl OnProcessSpell(void* AI, PSDK_SPELL_CAST SpellCast, void* UserData);
	static bool __cdecl OnCreate(void* Object, unsigned int NetworkID, void* UserData);
	static bool __cdecl OnDelete(void* Object, unsigned int NetworkID, void* UserData);

public:

	Collision();
	~Collision() {};

	bool CheckWallCollision(Vector2& from, Vector2& to, float width);
	bool CheckWallCollision(Vector3& from, Vector3& to, float width);

	bool CheckHeroCollision(Vector2& from, Vector2& to, float width, float delay, float missileSpeed = HUGE_VALF, bool AllyCollision = false, bool isArc = false);
	bool CheckHeroCollision(Vector3& from, Vector3& to, float width, float delay, float missileSpeed = HUGE_VALF, bool AllyCollision = false, bool isArc = false);

	bool CheckMinionCollision(Vector2& from, Vector2& to, float width, float delay, float missileSpeed = HUGE_VALF, bool AllyCollision = false, bool isArc = false);
	bool CheckMinionCollision(Vector3& from, Vector3& to, float width, float delay, float missileSpeed = HUGE_VALF, bool AllyCollision = false, bool isArc = false);

	bool CheckYasuoWallCollision(Vector2& from, Vector2& to, float width, bool isArc = false);
	bool CheckYasuoWallCollision(Vector3& from, Vector3& to, float width, bool isArc = false);

	bool CheckCollisions(Vector2& from, Vector2& to, float width, float delay, float missileSpeed = HUGE_VALF, bool AllyCollision = false, CollisionFlags CollisionFlags = CollisionFlags::Default, bool isArc = false);
	bool CheckCollisions(Vector3& from, Vector3& to, float width, float delay, float missileSpeed = HUGE_VALF, bool AllyCollision = false, CollisionFlags CollisionFlags = CollisionFlags::Default, bool isArc = false);

	std::shared_ptr<ICollision::Output> GetCollisions(Vector2& from, Vector2& to, float width, float delay, float missileSpeed = HUGE_VALF, bool AllyCollision = false, CollisionFlags CollisionFlags = CollisionFlags::Default, bool isArc = false);
	std::shared_ptr<ICollision::Output> GetCollisions(Vector3& from, Vector3& to, float width, float delay, float missileSpeed = HUGE_VALF, bool AllyCollision = false, CollisionFlags CollisionFlags = CollisionFlags::Default, bool isArc = false);
		
	bool CheckYasuoWallCollisionEx(Vector2& from, Vector2& to, float width, float delay, float missileSpeed = HUGE_VALF, bool AllyCollision = false, bool isArc = false);
	bool CheckYasuoWallCollisionEx(Vector3& from, Vector3& to, float width, float delay, float missileSpeed = HUGE_VALF, bool AllyCollision = false, bool isArc = false);
};