#include "Collision.h"

std::vector<Collision::YasuoWallData> Collision::YasuoWalls;

Collision::Collision() {
	pSDK->EventHandler->RegisterCallback(CallbackEnum::SpellCastStart, Collision::OnProcessSpell);
	pSDK->EventHandler->RegisterCallback(CallbackEnum::CreateObject, Collision::OnCreate);
	pSDK->EventHandler->RegisterCallback(CallbackEnum::DeleteObject, Collision::OnDelete);
}

bool Collision::CheckWallCollision(Vector2& from, Vector2& to, float width) {
	float h{ from.GetTerrainHeight() };
	Vector3 From{ from.To3D(h) };
	Vector3 To{ to.To3D(h) };
	return CheckWallCollision(From, To, width);
}
bool Collision::CheckWallCollision(Vector3& from, Vector3& to, float width) {
	float dist{ from.Distance(to) };
	float step{ std::max<float>(dist / 20.0f, 50.0f) };
	auto dir{ (from - to).Normalized() };
	auto perp{ dir.Perpendicular().Normalized() * width * 0.5f };	

	size_t max_steps { static_cast<size_t>(std::ceil(dist / step)) };
	for (size_t i = 0; i <= max_steps; i++) {

		float extendDist{ std::min<float>(step * i, dist) };
		auto  centerPos{ to + (dir * extendDist) };		

		if (centerPos.IsWall() || (centerPos + perp).IsWall() || (centerPos - perp).IsWall()) {
			return true;
		}	
	}
	return false;
}

bool Collision::CheckHeroCollision(Vector2 & from, Vector2 & to, float width, float delay, float missileSpeed, bool AllyCollision, bool isArc) {
	float distanceBetweenPoints{ from.Distance(to) + (width * 2.0f) };
	auto position { from.To3D() };
	std::map<unsigned int, AIHeroClient*> Objects{
		(AllyCollision ?
		pSDK->EntityManager->GetAllyHeroes(distanceBetweenPoints, &position) :
		pSDK->EntityManager->GetEnemyHeroes(distanceBetweenPoints, &position))
	};

	Paths arc{ isArc ? ClipperWrapper::DefineDianaArc(from, to, width).ToClipperPath() : Path() };
	for (auto &[_, Enemy] : Objects) {		
		auto bR{ Enemy->GetBoundingRadius() };
		auto pos{ pSDK->Prediction->GetFastPrediction(Enemy, delay, missileSpeed, &from) };			

		if (isArc) {
			if (pos.IsLeftOfLineSegment(from, to)) { continue; }

			Paths hitBox{ Geometry::Circle(&pos, bR + 15.0f).ToClipperPath() };
			if (ClipperWrapper::IsIntersects(hitBox, arc)) {
				return true;
			}
		}
		else {			
			auto projection{ pos.ProjectOn(from, to) };
			if (projection.IsOnSegment && projection.SegmentPoint.Distance(pos) <= (width + bR)) {
				return true;
			}
		}		
	}
	return false;		
}
bool Collision::CheckHeroCollision(Vector3 & from, Vector3 & to, float width, float delay, float missileSpeed, bool AllyCollision, bool isArc) {
	auto From{ from.To2D() };
	auto To{ to.To2D() };
	return CheckHeroCollision(From, To, width, delay, missileSpeed, AllyCollision, isArc);
}

bool Collision::CheckMinionCollision(Vector2& from, Vector2& to, float width, float delay, float missileSpeed, bool AllyCollision, bool isArc) {
	float distanceBetweenPoints{ from.Distance(to) + (width * 2.0f) };
	auto position { from.To3D() };
	std::map<unsigned int, AIMinionClient*> Objects{ 
		(AllyCollision ? 
		pSDK->EntityManager->GetAllyMinions (distanceBetweenPoints, &position) : 
		pSDK->EntityManager->GetEnemyMinions(distanceBetweenPoints, &position)
		) 
	};

	Paths arc{ isArc ? ClipperWrapper::DefineDianaArc(from, to, width).ToClipperPath() : Path() };
	for (auto &[_, Enemy] : Objects) {
		if (Enemy->IsWard()) { continue; }
		auto bR{ Enemy->GetBoundingRadius() };
		auto pos{ pSDK->Prediction->GetFastPrediction(Enemy, delay, missileSpeed, &from) };

		if (isArc) {
			if (pos.IsLeftOfLineSegment(from, to)) { continue; }

			Paths hitBox{ Geometry::Circle(&pos, bR + 15.0f).ToClipperPath() };
			if (ClipperWrapper::IsIntersects(hitBox, arc)) {
				return true;
			}
		}
		else {			
			auto projection{ pos.ProjectOn(from, to) };
			if (projection.IsOnSegment && projection.SegmentPoint.Distance(pos) <= (width + bR)) {
				return true;
			}
		}
	}
	return false;
}
bool Collision::CheckMinionCollision(Vector3& from, Vector3& to, float width, float delay, float missileSpeed, bool AllyCollision, bool isArc) {
	auto From{ from.To2D() };
	auto To{ to.To2D() };
	return CheckMinionCollision(From, To, width, delay, missileSpeed, AllyCollision, isArc);
}

bool Collision::CheckYasuoWallCollision(Vector2 & from, Vector2 & to, float width, bool isArc) {
	if (YasuoWalls.empty()) { return false; }

	for (auto &Wall : YasuoWalls) {
		if (GetTickCount() - Wall.CastTick > 4000 || Wall.Object == NULL)
			continue;

		auto Caster{ Wall.Object->GetParticleOwner() };
		if (!Caster || !Caster->IsEnemy())
			continue;

		auto wallPos{ Wall.Object->GetPosition().To2D() };
		Vector2 yasuoWallDirection = (wallPos - Wall.StartPosition).Normalized().Perpendicular();
		float yasuoWallWidth = 350.0f + 50.0f * Wall.Level;

		Vector2 yasuoWallStart = wallPos + (yasuoWallWidth * 0.5f) * yasuoWallDirection;
		Vector2 yasuoWallEnd = yasuoWallStart - yasuoWallWidth * yasuoWallDirection;

		Paths yasuoWallPoly{ Geometry::Rectangle(yasuoWallStart, yasuoWallEnd, 50.0f).ToClipperPath() };
		if (isArc) {
			Paths spellHitBox{ ClipperWrapper::DefineDianaArc(from, to, width).ToClipperPath() };
			if (ClipperWrapper::IsIntersects(yasuoWallPoly, spellHitBox)) {
				return true;
			}
		}
		else {
			Paths spellHitBox{ Geometry::Rectangle(from, to, width).ToClipperPath() };
			if (ClipperWrapper::IsIntersects(yasuoWallPoly, spellHitBox)) {
				return true;
			}
		}
	}	
	return false;
}
bool Collision::CheckYasuoWallCollisionEx(Vector2 & from, Vector2 & to, float width, float delay, float missileSpeed, bool AllyCollision, bool isArc) {
	if (YasuoWalls.empty()) { return false; }

	int TeamToLookFor{AllyCollision ? TEAM_ALLY : TEAM_ENEMY};
	for (auto &Wall : YasuoWalls) {
		unsigned int ticksSinceLastCast{ GetTickCount() - Wall.CastTick };

		if (GetTickCount() - Wall.CastTick > 4000 || Wall.Object == NULL) 
			continue;

		auto Caster{ Wall.Object->GetParticleOwner() };
		if (!Caster || Caster->GetTeamID() != TeamToLookFor)
			continue;			

		auto wallPos{ Wall.Object->GetPosition().To2D() };

		unsigned int ticksUntilReachWall{ (unsigned int)std::ceil((delay + wallPos.Distance(from) / missileSpeed) * 1000) };
		if ((ticksSinceLastCast + ticksUntilReachWall) > 4000) {
			continue;
		}

		Vector2 yasuoWallDirection = (wallPos - Wall.StartPosition).Normalized().Perpendicular();
		float yasuoWallWidth = 350.0f + 50.0f * Wall.Level;

		Vector2 yasuoWallStart = wallPos + (yasuoWallWidth * 0.5f) * yasuoWallDirection;
		Vector2 yasuoWallEnd = yasuoWallStart - yasuoWallWidth * yasuoWallDirection;		

		Paths yasuoWallPoly{ Geometry::Rectangle(yasuoWallStart, yasuoWallEnd, 50.0f).ToClipperPath() };
		if (isArc) {
			Paths spellHitBox{ ClipperWrapper::DefineDianaArc(from, to, width).ToClipperPath() };
			if (ClipperWrapper::IsIntersects(yasuoWallPoly, spellHitBox)) {
				return true;
			}
		}
		else {
			Paths spellHitBox{ Geometry::Rectangle(from, to, width).ToClipperPath() };
			if (ClipperWrapper::IsIntersects(yasuoWallPoly, spellHitBox)) {
				return true;
			}
		}
	}
	return false;
}
bool Collision::CheckYasuoWallCollision(Vector3 & from, Vector3 & to, float width, bool isArc) {
	auto From{ from.To2D() };
	auto To{ to.To2D() };
	return CheckYasuoWallCollision(From, To, width, isArc);
}
bool Collision::CheckYasuoWallCollisionEx(Vector3 & from, Vector3 & to, float width, float delay, float missileSpeed, bool AllyCollision, bool isArc) {
	auto From{ from.To2D() };
	auto To{ to.To2D() };
	return CheckYasuoWallCollisionEx(From, To, width, delay, missileSpeed, AllyCollision, isArc);
}

bool Collision::CheckCollisions(Vector2& from, Vector2& to, float width, float delay, float missileSpeed, bool AllyCollision,   CollisionFlags _CollisionFlags, bool isArc) {	
	return (
		   ((_CollisionFlags & CollisionFlags::Minions)   && CheckMinionCollision(from, to, width, delay, missileSpeed, AllyCollision, isArc))
		|| ((_CollisionFlags & CollisionFlags::Heroes)    && CheckHeroCollision  (from, to, width, delay, missileSpeed, AllyCollision, isArc))
		|| ((_CollisionFlags & CollisionFlags::YasuoWall) && CheckYasuoWallCollisionEx(from, to, width, delay, missileSpeed, AllyCollision, isArc))
		|| ((_CollisionFlags & CollisionFlags::Wall)	  && CheckWallCollision(from, to, width))
	);
}
bool Collision::CheckCollisions(Vector3 & from, Vector3 & to, float width, float delay, float missileSpeed, bool AllyCollision, CollisionFlags _CollisionFlags, bool isArc) {
	auto From{ from.To2D() };
	auto To{ to.To2D() };
	return CheckCollisions(From, To, width, delay, missileSpeed, AllyCollision, _CollisionFlags, isArc);
}

std::shared_ptr<ICollision::Output> Collision::GetCollisions(Vector2& from, Vector2& to, float width, float delay, float missileSpeed, bool AllyCollision, CollisionFlags _CollisionFlags, bool isArc) {
	float distanceBetweenPoints{ from.Distance(to) + (width * 2.0f) };
	Paths arc{ isArc ? ClipperWrapper::DefineDianaArc(from, to, width).ToClipperPath() : Path() };

	std::shared_ptr<ICollision::Output> result = std::make_shared<ICollision::Output>();
	
	if (_CollisionFlags & CollisionFlags::Minions) {
		auto position { from.To3D() };
		std::map<unsigned int, AIMinionClient*> Objects{
			(AllyCollision ?
			pSDK->EntityManager->GetAllyMinions(distanceBetweenPoints, &position) :
			pSDK->EntityManager->GetEnemyMinions(distanceBetweenPoints, &position)
			)
		};

		for (auto &[_, Enemy] : Objects) {
			if (Enemy->IsWard() || !Enemy->IsAlive()) { continue; }

			auto bR{ Enemy->GetBoundingRadius() };
			auto pos{ pSDK->Prediction->GetFastPrediction(Enemy, delay, missileSpeed, &from) };

			if (isArc) {
				if (pos.IsLeftOfLineSegment(from, to)) { continue; }

				Paths hitBox{ Geometry::Circle(&pos, bR + 15.0f).ToClipperPath() };
				if (ClipperWrapper::IsIntersects(hitBox, arc)) {
					result->Units.push_back(Enemy);
					result->Objects = (result->Objects | CollisionFlags::Minions);
				}
			}
			else {
				auto projection{ pos.ProjectOn(from, to) };
				if (projection.IsOnSegment && projection.SegmentPoint.Distance(pos) <= (width + bR)) {
					result->Units.push_back(Enemy);
					result->Objects = (result->Objects | CollisionFlags::Minions);
				}
			}
		}		
	}

	if ((_CollisionFlags & CollisionFlags::Heroes)) {		
		auto position { from.To3D() };
		std::map<unsigned int, AIHeroClient*> Objects{ 
			(AllyCollision ? 
			pSDK->EntityManager->GetAllyHeroes (distanceBetweenPoints, &position) : 
			pSDK->EntityManager->GetEnemyHeroes(distanceBetweenPoints, &position)
			) 
		};
		
		for (auto &[_, Enemy] : Objects) {
			if (!Enemy->IsVisible() || !Enemy->IsAlive()) { continue; }

			auto bR{ Enemy->GetBoundingRadius() };
			auto pos{ pSDK->Prediction->GetFastPrediction(Enemy, delay, missileSpeed, &from) };

			if (isArc) {
				if (pos.IsLeftOfLineSegment(from, to)) { continue; }

				Paths hitBox{ Geometry::Circle(&pos, bR + 15.0f).ToClipperPath() };
				if (ClipperWrapper::IsIntersects(hitBox, arc)) {
					result->Units.push_back(Enemy);
					result->Objects = (result->Objects | CollisionFlags::Heroes);
				}
			}
			else {
				auto projection{ pos.ProjectOn(from, to) };
				if (projection.IsOnSegment && projection.SegmentPoint.Distance(pos) <= (width + bR)) {
					result->Units.push_back(Enemy);
					result->Objects = (result->Objects | CollisionFlags::Heroes);
				}
			}
		}
	}

	if ((_CollisionFlags & CollisionFlags::Wall) && CheckWallCollision(from, to, width))
		result->Objects = (result->Objects | CollisionFlags::Wall);

	if ((_CollisionFlags & CollisionFlags::YasuoWall) && CheckYasuoWallCollisionEx(from, to, width, delay, missileSpeed, AllyCollision))
		result->Objects = (result->Objects | CollisionFlags::YasuoWall);

	return result;
}
std::shared_ptr<ICollision::Output> Collision::GetCollisions(Vector3 & from, Vector3 & to, float width, float delay, float missileSpeed, bool AllyCollision, CollisionFlags _CollisionFlags, bool isArc) {
	auto From{ from.To2D() };
	auto To{ to.To2D() };
	return GetCollisions(From, To, width, delay, missileSpeed, AllyCollision, _CollisionFlags, isArc);
}

#pragma region YasuoWall
void Collision::OnProcessSpell(void* AI, PSDK_SPELL_CAST SpellCast, void* UserData) {
	UNREFERENCED_PARAMETER(UserData);

	if (!SpellCast || SpellCast->IsAutoAttack || !AI || !SDKSTATUS_SUCCESS(SdkIsObjectHero(AI)) || SpellCast->Spell.ScriptName == NULL)
		return;

	auto Sender = pSDK->EntityManager->GetObjectFromPTR(AI);
	if (Sender == NULL) { return; }

	if (strcmp(SpellCast->Spell.ScriptName, "YasuoW") == 0) {
		YasuoWalls.push_back({ NULL, GetTickCount(), SpellCast->Spell.Level, SpellCast->StartPosition });
	}		
}
bool __cdecl Collision::OnCreate(void* Object, unsigned int NetworkID, void* UserData) {
	UNREFERENCED_PARAMETER(UserData);

	if (Object == NULL || NetworkID == NULL || YasuoWalls.empty()) { return true; }

	auto obj{ pSDK->EntityManager->GetObjectFromID(NetworkID) };
	if (obj && obj->IsParticle()) {
		if (strstr(obj->GetName(), "_W_windwall")) {
			YasuoWalls.back().Object = obj;
		}
	}

	return true;
}
bool __cdecl Collision::OnDelete(void* Object, unsigned int NetworkID, void* UserData) {
	UNREFERENCED_PARAMETER(NetworkID);
	UNREFERENCED_PARAMETER(UserData);

	if (Object == NULL || YasuoWalls.empty()) { return true; }

	for (auto it = YasuoWalls.begin(); it != YasuoWalls.end();) {
		auto &Data{ *it };

		if (Data.Object == Object) {
			it = YasuoWalls.erase(it);
		}
		else {
			++it;
		}
	}
	return true;
}
#pragma endregion