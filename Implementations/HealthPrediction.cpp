#include "HealthPrediction.h"

unsigned long HealthPred::LastCleanUp = 0;
std::map<unsigned int, std::forward_list<std::shared_ptr<AutoAttack> >> Attacks::InboundAttacks;
std::map<unsigned int, unsigned int> MeleeTargets;
std::map<unsigned int, unsigned int> LastFakeAttack;

#pragma region Attacks
__declspec(selectany) std::map<std::string, const char *> specialMissileSpeeds{
	{std::string("Azir")   , NULL},
	{std::string("Velkoz") , NULL},
	{std::string("Thresh") , NULL},
	{std::string("Rakan")  , NULL},
	{std::string("Kayle")  , "JudicatorRighteousFury"},
	{std::string("Viktor") , "ViktorPowerTransferReturn"},
};

float GetMissileSpeed(MissileClient* Missile) {
	if (Missile == NULL || !Missile->IsValid()) { return 0.0f; };

	auto hero = Missile->GetOwner();
	if (!hero || hero->IsMelee()) {
		return HUGE_VALF;
	}

	if (specialMissileSpeeds.find(hero->GetCharName()) != specialMissileSpeeds.end()) {
		const char * buffName = specialMissileSpeeds[hero->GetCharName()];
		if (buffName == NULL || hero->HasBuff(buffName)) {
			return HUGE_VALF;
		}
	}

	return max(Missile->GetMissileSpeed(), hero->GetAttackData().MissileSpeed);
}

AutoAttack::AutoAttack(AIBaseClient * Sender, AIBaseClient * Target) {
	this->AttackStatus = AttackState::Detected;
	this->DetectTime = Common::GetTickCount() - Game::Ping() / 2;
	this->Sender = Sender;
	this->Target = Target;
	this->SNetworkId = Sender->GetNetworkID();
	this->NetworkId = Target->GetNetworkID();

	this->Damage = pSDK->DamageLib->GetAutoAttackDamage(Sender, Target, false);
	this->StartPosition = Sender->GetPosition();
	this->AttackDelay = (unsigned int)(Sender->GetAttackDelay() * 1000.0f);
	this->AttackCastDelay = (unsigned int)(Sender->GetAttackCastDelay() * 1000.0f);
}

bool AutoAttack::CanRemoveAttack() {
	return (Common::GetTickCount() - this->DetectTime > 2000);
}

float AutoAttack::GetPredictedDamage(int Delay) {
	auto sender{ (AIBaseClient*)pSDK->EntityManager->GetObjectFromID(SNetworkId) };
	auto target{ (AIBaseClient*)pSDK->EntityManager->GetObjectFromID(NetworkId) };

	if (!sender || !sender->IsAlive() || !sender->IsVisible() || sender->IsMoving() || !target || !target->IsAlive() || !target->IsVisible()) {
		return 0.0f;
	}

	float damage = 0.f;
	int delay = Delay;

	if ((DetectTime + AttackDelay) >= (Common::GetTickCount() - 100)) {
		delay += Game::Ping() - 100;
		auto timeToHit = this->TimeToLand();

		int attackAmount = 0;
		while ((int)timeToHit < delay) {
			if (timeToHit > 0) {
				++attackAmount;
			}
			timeToHit += AttackDelay;
		}

		damage += Damage * attackAmount;
	}
	return damage;
}

float AutoAttack::Distance() {
	auto pos{ this->Target->GetPosition() };
	return this->IsValid() ? this->StartPosition.Distance(pos) - this->Sender->GetBoundingRadius() - this->Target->GetBoundingRadius() : 0.0f;
}

int AutoAttack::ETA() {
	return this->LandTime() - (int)Common::GetTickCount();
}

unsigned int AutoAttack::ElapsedTime() {
	return Common::GetTickCount() - this->DetectTime;
}

RangedAttack::RangedAttack(AIBaseClient * sender, AIBaseClient * target, MissileClient* mc, int extraDelay) : AutoAttack(sender, target) {
	this->ExtraDelay = extraDelay;
	this->misNID  = mc->GetNetworkID();
	this->Missile = mc;
	this->StartPosition = mc->GetStartPos();
	this->IsMelee = false;
	this->MissileSpeed = GetMissileSpeed(this->Missile);
}

bool RangedAttack::IsValid() {
	if (!pSDK->EntityManager->IsValidObj(this->misNID) || !pSDK->EntityManager->IsValidObj(this->SNetworkId) || 
		!pSDK->EntityManager->IsValidObj(this->NetworkId)  || 
		this->Missile == NULL || !this->Missile->IsValid() || 
		this->Sender  == NULL || !this->Sender->IsValid()  || !this->Sender->IsAlive() || 
		this->Target  == NULL || !this->Target->IsValid()  || !this->Target->IsAlive()) 
	{
		return false;
	}	
	return this->AttackStatus != AutoAttack::AttackState::Completed;
}

int RangedAttack::LandTime() {
	return (int)Common::GetTickCount() + TimeToLand();
}

int RangedAttack::TimeToLand() {
	if (!this->IsValid()) { return 0; }
	auto position { this->Missile->GetEndPos() };
	return (int)((this->Missile->Distance(&position) / MissileSpeed) * 1000.0f) + this->ExtraDelay;
}

bool RangedAttack::HasReached() {	
	return (!this->IsValid() || this->AttackStatus == AttackState::Completed || this->ETA() < -200);
}

MeleeAttack::MeleeAttack(AIBaseClient* sender, AIBaseClient* target, int extraDelay) : AutoAttack(sender, target) {
	this->ExtraDelay = extraDelay + 100;
	this->misNID = NULL;
	this->Missile = NULL;
	this->IsMelee = true;
}

bool MeleeAttack::IsValid() {
	if (!pSDK->EntityManager->IsValidObj(this->SNetworkId) || !pSDK->EntityManager->IsValidObj(this->NetworkId) ||
		this->Sender == NULL || !this->Sender->IsValid() || !this->Sender->IsAlive() || this->Target == NULL || !this->Target->IsValid() || !this->Target->IsAlive()) {
		return false;
	}
	return this->AttackStatus != AttackState::Completed;
}

int MeleeAttack::LandTime() {
	return this->IsValid() ? (int)this->DetectTime + (int)(AttackCastDelay) + this->ExtraDelay : (int)(Common::GetTickCount() - 100);
}

int MeleeAttack::TimeToLand() {
	return LandTime() - (int)Common::GetTickCount();
}

bool MeleeAttack::HasReached() {
	return (this->AttackStatus == AttackState::Completed || !this->IsValid() || this->ETA() < -100);
}

std::forward_list<std::shared_ptr<AutoAttack>>& Attacks::GetInboundAttacks(unsigned int TargetID) {
	if (InboundAttacks.find(TargetID) == InboundAttacks.end()) {
		InboundAttacks[TargetID] = std::forward_list<std::shared_ptr<AutoAttack>>{};
	}
	return InboundAttacks[TargetID];
}

void Attacks::AddMeleeAttack(AIBaseClient * sender, AIBaseClient * target, int extraDelay) {
	if (InboundAttacks.find(target->GetNetworkID()) == InboundAttacks.end()) {
		InboundAttacks[target->GetNetworkID()] = std::forward_list<std::shared_ptr<AutoAttack>>{};
	}
	InboundAttacks[target->GetNetworkID()].push_front(std::make_shared<MeleeAttack>(sender, target, extraDelay));
}

void Attacks::AddRangedAttack(AIBaseClient * sender, AIBaseClient * target, MissileClient * mc, int extraDelay) {
	if (InboundAttacks.find(target->GetNetworkID()) == InboundAttacks.end()) {
		InboundAttacks[target->GetNetworkID()] = std::forward_list<std::shared_ptr<AutoAttack>>{};
	}
	InboundAttacks[target->GetNetworkID()].push_front(std::make_shared<RangedAttack>(sender, target, mc, extraDelay));
}
#pragma endregion

HealthPred::HealthPred() {
	pSDK->EventHandler->RegisterCallback(CallbackEnum::Tick, Tick);
	pSDK->EventHandler->RegisterCallback(CallbackEnum::CreateObject, OnCreate);
	pSDK->EventHandler->RegisterCallback(CallbackEnum::DeleteObject, OnDelete);
	pSDK->EventHandler->RegisterCallback(CallbackEnum::SpellCastEnd, OnCastSpell);
	pSDK->EventHandler->RegisterCallback(CallbackEnum::Attack, OnProcessAttack);
	pSDK->EventHandler->RegisterCallback(CallbackEnum::NewPath, MeleeMinionHelper);	

	if (DevMode) {
		pSDK->EventHandler->RegisterCallback(CallbackEnum::Overlay, HealthPred::DrawMenu, NULL);
		pSDK->EventHandler->RegisterCallback(CallbackEnum::Update, HealthPred::Draw, NULL);
	}
}

unsigned int HealthPred::CalculateMissileTravelTime(AIBaseClient* Target, Vector3 * StartPos, float MissileDelay, float MissileSpeed) {
	if (MissileSpeed <= EPSILON || MissileSpeed == HUGE_VALF || Target == NULL || StartPos == NULL) {
		return 0;
	}

	auto minionPosition = Target->GetPosition();
	auto navInfo = Target->NavInfo();

	if (!Target->IsMoving() || navInfo.NumberOfWaypoints == 2) {
		return (unsigned int)(StartPos->Distance(minionPosition) / MissileSpeed * 1000.0f);
	}

	auto direction = (navInfo.EndPos - minionPosition).Normalized();
	auto totalPathLength = navInfo.EndPos.Distance(minionPosition);
	auto delayDistance = MissileDelay * Target->GetMovementSpeed();

	if (delayDistance <= totalPathLength) {
		auto positionAfterDelay = minionPosition + direction * delayDistance;
		auto dir{ direction.To2D() };
		auto angle = (positionAfterDelay - *StartPos).To2D().Normalized().AngleBetween(dir);
		auto c = std::sin(Target->GetMovementSpeed() / MissileSpeed * std::sin(angle));

		if (M_PI - angle - std::asin(c) >= 0) {
			auto playerDistance = (float)(c / std::sin(M_PI - angle - std::asin(c))	* positionAfterDelay.To2D().Distance(*StartPos));
			auto predictedPos = positionAfterDelay + direction * playerDistance;

			minionPosition = (playerDistance + delayDistance <= totalPathLength) ? predictedPos : navInfo.EndPos;
		}
		else {
			minionPosition = navInfo.EndPos;
		}
	}
	else {
		minionPosition = navInfo.EndPos;
	}
	
	return (unsigned int)(StartPos->Distance(minionPosition) / MissileSpeed * 1000.0f);
}

float HealthPred::GetDamagePrediction(AIBaseClient* Target, unsigned int Time, bool SimulateDmg) {
	if (Target == NULL) { return 0.0f; }

	float predictedDamage = 0;

	for (auto Attack : Attacks::GetInboundAttacks(Target->GetNetworkID())) {
		if (Attack == nullptr || (!SimulateDmg && Attack->HasReached()) || Attack->ETA() > (int)Time) {
			continue;
		}

		predictedDamage += SimulateDmg ? Attack->GetPredictedDamage((int)Time) : Attack->Damage;		
	}
	return predictedDamage;
}
float HealthPred::GetHealthPrediction(AIBaseClient * Target, unsigned int Time, bool SimulateDmg) {
	if (Target == NULL) { return 0.0f; }
	return Target->GetHealth().Current - GetDamagePrediction(Target, Time, SimulateDmg);
}
bool  HealthPred::CanKillMinion(AIBaseClient * Minion, float missileDelay, float missileSpeed) {
	auto position { Player.GetPosition() };
	auto travelTime{ this->CalculateMissileTravelTime(Minion, &position, missileDelay, missileSpeed )};
	auto healthPred{ pSDK->HealthPred->GetHealthPrediction(Minion, travelTime, false) };
	return healthPred > 0.0f && healthPred < pSDK->DamageLib->GetAutoAttackDamage(&Player, Minion, true);
}

#pragma region Callbacks
void __cdecl HealthPred::Draw(void * UserData) {
	UNREFERENCED_PARAMETER(UserData);

	std::vector<AIBaseClient*> Objects{};
	auto Minions{ pSDK->EntityManager->GetEnemyMinions(1500.0f) };
	for (auto &[_, obj] : Minions) { if (obj->IsAlive()) Objects.push_back(obj); }
	auto Heroes{ pSDK->EntityManager->GetEnemyHeroes(1500.0f) };
	for (auto &[_, obj] : Heroes) { if (obj->IsAlive()) Objects.push_back(obj); }
	Objects.push_back(&Player);

	auto staticDmg{ pSDK->DamageLib->GetStaticAutoAttackDamage(&Player, true) };
	for (auto &obj : Objects) {
		if (!obj->IsMouseOver()) { continue; }

		size_t i = 20;
		auto screenPos{ Renderer::WorldToScreen(obj->GetPosition()) };
		screenPos.y += 60.0f;
		if (Menu::Get<bool>("HealthPred.Menu.DrawDmg")) {
			std::string str{ "AA Dmg: " + std::to_string(pSDK->DamageLib->GetAutoAttackDamage(&Player, obj, &staticDmg, true)) };
			screenPos.y += i;
			Draw::Text(NULL, &screenPos, str);
		}
		if (Menu::Get<bool>("HealthPred.Menu.DrawIncomingDmg")) {
			std::string str{ "Incoming Dmg: " + std::to_string(pSDK->HealthPred->GetDamagePrediction(obj, 2000, false)) };
			std::string str2{ "Incoming Dmg(Simulated): " + std::to_string(pSDK->HealthPred->GetDamagePrediction(obj, 2000, true)) };
			screenPos.y += i;
			Draw::Text(NULL, &screenPos, str);
			screenPos.y += i;
			Draw::Text(NULL, &screenPos, str2);
		}
	}

	if (Menu::Get<bool>("HealthPred.Menu.DrawBuffs")) {
		auto Allies{ pSDK->EntityManager->GetAllyHeroes() };
		Heroes.insert(Allies.begin(), Allies.end());

		for (auto &[_, Hero] : Heroes) {
			if (!Hero->IsVisibleOnScreen()) { continue; }

			auto screenPos{ Renderer::WorldToScreen(Hero->GetPosition()) };
			screenPos.y += 30;
			for (auto & Buff : Hero->GetBuffs()) {
				std::string str{ "buffName: " + Buff.Name + " Stacks: " + std::to_string(Buff.Stacks) + " Count: " + std::to_string(Buff.HasCount ? Buff.Count : 0) };
				screenPos.y += 20;
				Draw::Text(NULL, &screenPos, str);
			}
		}
	}
}
void __cdecl HealthPred::DrawMenu(void * UserData) {
	UNREFERENCED_PARAMETER(UserData);

	Menu::Tree("HealthPred", "HealthPred.Menu.Tree", false, []() {
		Menu::Checkbox("Print Damage Calc", "HealthPred.Menu.PrintDmg", true);
		Menu::Checkbox("Draw Damage Calc",  "HealthPred.Menu.DrawDmg", true);
		Menu::Checkbox("Draw Incoming Dmg", "HealthPred.Menu.DrawIncomingDmg", true);
		Menu::Checkbox("Draw Player Buffs", "HealthPred.Menu.DrawBuffs", true);
	});
}

void HealthPred::Tick(void * UserData) {
	UNREFERENCED_PARAMETER(UserData);

	//Limit the clean up to every half seconds
	if (GetTickCount() - LastCleanUp <= 500) {
		return;
	}

	for (auto&[nID, AttackList] : Attacks::InboundAttacks) {
		AttackList.remove_if([](std::shared_ptr<AutoAttack> Attack) {
			return Attack == nullptr || Attack->CanRemoveAttack();
		});
	}
	LastCleanUp = GetTickCount();
}

bool HealthPred::OnCreate(void* Object, unsigned int NetworkID, void* UserData) {
	UNREFERENCED_PARAMETER(UserData);

	if (!Object || !SDKSTATUS_SUCCESS(SdkIsObjectSpellMissile(Object)) || !pSDK->EntityManager->IsValidObj(NetworkID))
		return true;

	auto Missile = (MissileClient*)pSDK->EntityManager->GetObjectFromPTR(Object);
	if (Missile == NULL)
		return true;

	auto sender = Missile->GetOwner();
	auto target = Missile->GetTarget();

	if (sender == NULL || target == NULL || !sender->IsValid() || !target->IsValid() || !target->IsAlive() || !sender->IsAlive() ||
		sender->GetNetworkID() == Player.GetNetworkID() || (target->GetTeamID() != 300 && sender->Distance(&Player) > 2000.0f)) {
		return true;
	}

	if (target->IsHero() || target->IsMinion() || target->IsTurret()) {
		AIBaseClient* baseTarget = (AIBaseClient*)target;

		auto SenderNID{ sender->GetNetworkID() };
		for (auto&[nID, AttackList] : Attacks::InboundAttacks) {
			AttackList.remove_if([&SenderNID](std::shared_ptr<AutoAttack> Attack) {
				return Attack == nullptr || Attack->SNetworkId == SenderNID;
			});
		}

		Attacks::AddRangedAttack(sender, baseTarget, Missile, sender->IsTurret() ? 100 : 0);
	}
	return true;
}
bool HealthPred::OnDelete(void* Object, unsigned int NetworkID, void* UserData) {
	UNREFERENCED_PARAMETER(UserData);

	MeleeTargets.erase(NetworkID);
	LastFakeAttack.erase(NetworkID);

	if (!Object || !SDKSTATUS_SUCCESS(SdkIsObjectSpellMissile(Object)) || !pSDK->EntityManager->IsValidObj(NetworkID))
		return true;

	MissileClient* mc{ (MissileClient*)pSDK->EntityManager->GetObjectFromID(NetworkID) };
	if (mc == NULL || !mc->IsValid() || mc->GetOwner() == NULL || mc->GetTarget() == NULL || mc->Distance(&Player) > 3000.f) {  //|| mc.GetOwner()->IsHero() 
		return true;
	}

	for (auto&[nID, AttackList] : Attacks::InboundAttacks) {
		for (std::shared_ptr<AutoAttack>& Attack : AttackList) {
			if (!Attack) { continue; }

			if (Attack->misNID == NetworkID) {
				Attack->AttackStatus = AutoAttack::AttackState::Completed;
				return true;
			}
		}
	}
	return true;
}

void HealthPred::MeleeMinionHelper(void* AI, bool Move, bool Stop, void* UserData) {
	UNREFERENCED_PARAMETER(Move);
	UNREFERENCED_PARAMETER(UserData);

	if (AI == NULL || AI == Player.PTR()) {
		return;
	}	

	auto curTick{ Common::GetTickCount() };
	auto Sender = pSDK->EntityManager->GetObjectFromPTR(AI);
	if (Sender) {
		auto nID{ Sender->GetNetworkID() };

		if (Move) {
			LastFakeAttack.erase(nID);
			MeleeTargets.erase(nID);
		}

		if (Stop && Sender->Distance(&Player) < 2000.0f && Sender->IsMinion()) { 
			if (LastFakeAttack.count(nID) == 0) {
				LastFakeAttack[nID] = 0;
			}

			if ((curTick - LastFakeAttack[nID]) > 400) {
				if (MeleeTargets.count(nID) > 0) {
					auto tmpTarget{ pSDK->EntityManager->GetObjectFromID(MeleeTargets[nID]) };
					if (tmpTarget) {
						OnProcessAttack(AI, tmpTarget->PTR(), true, false, NULL);
					}
				}
			}

			LastFakeAttack[nID] = curTick;		
		}		
	}
}
void HealthPred::OnProcessAttack(void* AI, void* TargetObject, bool StartAttack, bool StopAttack, void* UserData) {
	UNREFERENCED_PARAMETER(StopAttack);
	UNREFERENCED_PARAMETER(UserData);
	if (AI == NULL || TargetObject == NULL) {
		return;
	}

	if (!StartAttack || !SDKSTATUS_SUCCESS(SdkIsObjectAI(TargetObject)))
		return;

	auto Sender = (AIBaseClient*)pSDK->EntityManager->GetObjectFromPTR(AI);
	if (Sender != NULL && Sender->IsAlive() && Sender->GetNetworkID() != Player.GetNetworkID() && Sender->Distance(&Player) < 2000.f) {		
		auto Target{ (AIBaseClient*)pSDK->EntityManager->GetObjectFromPTR(TargetObject) };
		if (Target != NULL) {
			auto SenderNID{ Sender->GetNetworkID() };

			for (auto&[nID, AttackList] : Attacks::InboundAttacks) {
				AttackList.remove_if([&SenderNID](std::shared_ptr<AutoAttack> Attack) {
					return Attack == nullptr || Attack->SNetworkId == SenderNID;
				});
			}

			if (Sender->IsMelee()) {
				MeleeTargets[SenderNID] = Target->GetNetworkID();
				LastFakeAttack[SenderNID] = Common::GetTickCount();				
				Attacks::AddMeleeAttack(Sender, Target, 0);
			}				
			else {
				auto time{ (int)std::floor(1000.0f * Sender->Distance(Target) / Sender->GetAttackData().MissileSpeed) };
				Attacks::AddMeleeAttack(Sender, Target, time);
			}
		}
	}	
}
void HealthPred::OnCastSpell(void* AI, PSDK_SPELL_CAST SpellCast, void* UserData) {
	UNREFERENCED_PARAMETER(UserData);

	if (!SpellCast || !SpellCast->IsAutoAttack || AI == NULL || SpellCast->TargetObject == NULL)
		return;

	AIBaseClient* Sender = (AIBaseClient*)pSDK->EntityManager->GetObjectFromPTR(AI);
	if (Sender != NULL && Sender->IsAlive() && Sender->IsMelee() && Sender->GetNetworkID() != Player.GetNetworkID() && Sender->Distance(&Player) < 2000.f) {
		auto NetworkID{ Sender->GetNetworkID() };
		for (auto&[nID, AttackList] : Attacks::InboundAttacks) {
			for (std::shared_ptr<AutoAttack>& Attack : AttackList) {
				if (!Attack) { continue; }

				if (Attack->SNetworkId == NetworkID) {
					Attack->AttackStatus = AutoAttack::AttackState::Completed;
					return;
				}
			}
		}
	}
}
#pragma endregion
