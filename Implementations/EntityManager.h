#pragma once

#pragma warning(push, 0)
#pragma warning(disable: 4365)
#include <map>
#pragma warning(pop)

#include "SDK Extensions.h"

#define ENTITY_GET_DECL(TYPE, NAME) \
static std::map<unsigned int, TYPE> m_##NAME; \
std::map<unsigned int, TYPE*> Get##NAME (float Range = HUGE_VALF, Vector3* From = NULL); \


class EntityManager : public IEntityManager {
public:	
	
	EntityManager();
	~EntityManager() {};
	   	 
	ENTITY_GET_DECL(AIMinionClient, AzirSoldiers);
	ENTITY_GET_DECL(AIHeroClient,	EnemyHeroes);	
	ENTITY_GET_DECL(AIHeroClient,	AllyHeroes);
	ENTITY_GET_DECL(AIMinionClient,	AllyMinions);	
	ENTITY_GET_DECL(AIMinionClient,	EnemyMinions);	
	ENTITY_GET_DECL(AITurretClient,	AllyTurrets);	
	ENTITY_GET_DECL(AITurretClient,	EnemyTurrets);	
	ENTITY_GET_DECL(MissileClient,	AllyMissiles);	
	ENTITY_GET_DECL(MissileClient,	EnemyMissiles);	
	ENTITY_GET_DECL(AIMinionClient,	JungleMonsters);	
	ENTITY_GET_DECL(AIMinionClient,	EnemyWards);		
	ENTITY_GET_DECL(AIMinionClient,	AllyWards);		
	ENTITY_GET_DECL(GameObject,		Particles);		
	ENTITY_GET_DECL(GameObject,		Shops);		
	ENTITY_GET_DECL(AttackableUnit,	Nexus);			
	ENTITY_GET_DECL(AttackableUnit,	Inhibitors);		

	AITurretClient * GetNearestTurret(AIBaseClient* Obj, Vector3* Pos, int Team = 0);

	bool IsValidObj(void * Obj);
	bool IsValidObj(unsigned int nID);

	GameObject* GetObjectFromPTR(void * Object);
	GameObject* GetObjectFromID (unsigned int ID);

	AIHeroClient GetLocalPlayer();
	
	static bool __cdecl CreateObj(void* Object, unsigned int NetworkID, void* UserData);
	static bool __cdecl DeleteObj(void* Object, unsigned int NetworkID, void* UserData);
	static bool __cdecl CheckObjects(void * Object, unsigned int NetworkID, void * UserData);	
	static void __cdecl DrawMenu (void* UserData);
private:	
	static std::map<unsigned int, GameObject*> m_ObjectPointers;
};






