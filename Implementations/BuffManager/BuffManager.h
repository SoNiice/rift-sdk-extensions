#pragma once

#include "SDK Extensions.h"

class BuffManager : public IBuffManager {
	static std::map<unsigned int, std::vector<PerkInstance>> PerksLookUp;	
	static std::map<unsigned int, std::map<std::string, BuffInstance>> BuffLookUp;
	static std::map<std::string, SDK_SPELL> BuffSpellLookUp;

	static void __cdecl BuffChange(void* AI, unsigned char Type, float StartTime, float EndTime, const char* Name, void* CasterObject, unsigned int CasterID, int Stacks, bool HasCount, int Count, PSDK_SPELL Spell, void* UserData);
	static void __cdecl BuffCreateDelete(void* AI, bool Created, unsigned char Type, float StartTime, float EndTime, const char* Name, void* CasterObject, unsigned int CasterID, int Stacks, bool HasCount, int Count, PSDK_SPELL Spell, void* UserData);
	static bool	__cdecl	InitializeExistingBuffs(void* Object, unsigned int NetworkID, void* UserData);
	static bool	__cdecl	OnDelete(void* Object, unsigned int NetworkID, void* UserData);
public:
	BuffManager();
	~BuffManager() {};

	bool HasBuff(unsigned int nID, std::string buffName, bool partialName = false) override;
	bool HasBuffType(unsigned int nID, unsigned char buffType) override;

	std::vector<BuffInstance> GetBuffs(unsigned int nID);
	BuffInstance GetBuff(unsigned int nID, std::string buffName, bool partialName = false) override;
	BuffInstance GetBuffByType(unsigned int nID, unsigned char buffType) override;

	int  GetBuffStacks(unsigned int nID, std::string buffName, bool partialName = false) override;
	int  GetBuffCount (unsigned int nID, std::string buffName, bool partialName = false) override;	

	std::vector<PerkInstance> GetPerks(AIHeroClient* Hero) override;
	bool HasPerk(AIHeroClient* Hero, unsigned int ID) override;
	bool HasPerk(AIHeroClient* Hero, std::string Name) override;		
};