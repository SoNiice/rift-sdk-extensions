#include "BuffManager.h"
#include "Common.hpp"

std::map<unsigned int, std::vector<PerkInstance>> BuffManager::PerksLookUp;
std::map<std::string, SDK_SPELL> BuffManager::BuffSpellLookUp;
std::map<unsigned int, std::map<std::string, BuffInstance>> BuffManager::BuffLookUp;

BuffManager::BuffManager() {
	pSDK->EventHandler->RegisterCallback(CallbackEnum::BuffCreateAndDelete, BuffManager::BuffCreateDelete, NULL);
	pSDK->EventHandler->RegisterCallback(CallbackEnum::BuffUpdate, BuffManager::BuffChange, NULL);
	pSDK->EventHandler->RegisterCallback(CallbackEnum::DeleteObject, BuffManager::OnDelete, NULL);

	SdkEnumHeroes(InitializeExistingBuffs, NULL);
	SdkEnumMinions(InitializeExistingBuffs, NULL);
	SdkEnumTurrets(InitializeExistingBuffs, NULL);
}

bool BuffManager::HasBuff(unsigned int nID, std::string buffName, bool partialName) {	
	return GetBuff(nID, buffName, partialName).IsValid();
}
bool BuffManager::HasBuffType(unsigned int nID, unsigned char buffType) {
	if (BuffLookUp.count(nID) > 0) {
		auto &ListOfBuffs{ BuffLookUp[nID] };
		for (auto &[Name, Buff] : ListOfBuffs) {
			if (Buff.IsValid() && Buff.Type == buffType) {
				return true;
			}
		}
	}
	return false;
}

std::vector<BuffInstance> BuffManager::GetBuffs(unsigned int nID) {
	std::vector<BuffInstance> result{};

	if (BuffLookUp.count(nID) > 0) {
		for (auto&[name, buff] : BuffLookUp[nID]) {
			if (buff.IsValid())
				result.push_back(buff);
		}
	}
	
	return result;
}

BuffInstance BuffManager::GetBuff(unsigned int nID, std::string buffName, bool partialName) {
	if (BuffLookUp.count(nID) > 0) {
		auto &ListOfBuffs{ BuffLookUp[nID] };
		Common::ToLower(&buffName);		

		if (!partialName) {
			if (ListOfBuffs.count(buffName) > 0) {
				if (ListOfBuffs[buffName].IsValid()) {
					return ListOfBuffs[buffName];
				}
				return { "", 0, "", 0.0f, 0.0f, false, 0, NULL, NULL, 0 };
			}
		}
		else {
			for (auto &[Name, Buff] : ListOfBuffs) {
				if (Buff.IsValid() && Name.find(buffName) != std::string::npos) {
					return Buff;
				}
			}
		}

	}	

	return { "", 0, "", 0.0f, 0.0f, false, 0, NULL, NULL, 0 };
}
BuffInstance BuffManager::GetBuffByType(unsigned int nID, unsigned char buffType) {	
	BuffInstance bestBuff{"", 0, "", 0.0f, 0.0f, false, 0, NULL, NULL, 0 };
	if (BuffLookUp.count(nID) > 0) {
		auto &ListOfBuffs{ BuffLookUp[nID] };	
		float bestDuration{ 0.0f };		

		for (auto &[Name, Buff] : ListOfBuffs) {
			if (Buff.IsValid() && Buff.Type == buffType && Buff.EndTime > bestDuration) {
				bestBuff = Buff;
				bestDuration = Buff.EndTime;
			}
		}
	}
	return bestBuff;
}

int BuffManager::GetBuffStacks(unsigned int nID, std::string buffName, bool partialName) {
	auto buff{ GetBuff(nID, buffName, partialName) };
	return buff.IsValid() ? buff.Stacks : 0;
}
int BuffManager::GetBuffCount(unsigned int nID, std::string buffName, bool partialName) {
	auto buff{ GetBuff(nID, buffName, partialName) };
	return (buff.IsValid() && buff.HasCount) ? buff.Count : 0;
}

std::vector<PerkInstance> BuffManager::GetPerks(AIHeroClient* Hero) {
	auto nID{ Hero->GetNetworkID() };
	if (PerksLookUp.count(nID) == 0) {
		std::vector<PerkInstance> output;

		SdkEnumHeroPerks(
			Hero->PTR(),
			[](unsigned int ID, const char* Name, void* UserData) {
				if (!UserData) {
					SdkUiConsoleWrite("[SDK] EnumPerks Error");
					return false;
				}
				std::vector<PerkInstance>* Perks = (std::vector<PerkInstance>*)(UserData);
				if (Perks) {
					Perks->push_back(PerkInstance{ ID, Name ? std::string(Name) : "" });
				}
				return true;
			},
			&output
		);

		PerksLookUp[nID] = output;
	}
	return PerksLookUp[nID];
}
bool BuffManager::HasPerk(AIHeroClient* Hero, unsigned int ID) {
	for (auto &Perk : GetPerks(Hero)) {
		if (Perk.ID == ID) {
			return true;
		}
	}
	return false;
}
bool BuffManager::HasPerk(AIHeroClient* Hero, std::string Name) {
	for (auto &Perk : GetPerks(Hero)) {
		if (Perk.Name == Name) {
			return true;
		}
	}
	return false;
}

void __cdecl BuffManager::BuffCreateDelete(
	void * AI, 
	bool Created, 
	unsigned char Type, 
	float StartTime, 
	float EndTime, 
	const char * Name, 
	void * CasterObject, 
	unsigned int CasterID, 
	int Stacks, 
	bool HasCount, 
	int Count, 
	PSDK_SPELL Spell, 
	void * UserData)
{
	UNREFERENCED_PARAMETER(CasterID);
	UNREFERENCED_PARAMETER(UserData);
		
	if (Name == NULL) 
		return;		

	std::string buffName{ Name };
	Common::ToLower(&buffName);
	unsigned int nID{};
	SdkGetObjectNetworkID(AI, &nID);
	if (!nID) { return; }

	if (Created) {		
		//Copies Spell struct
		PSDK_SPELL spellPTR{ NULL };
		if (Spell != NULL) {
			BuffSpellLookUp[buffName] = *(Spell);
			spellPTR = &BuffSpellLookUp[buffName];
		}			

		//Add Buff to Target AIs Map
		const char* typeStr{""};
		SdkBuffTypeToString(Type, &typeStr);
		BuffLookUp[nID][buffName] = { Name, Type, typeStr, StartTime, EndTime, HasCount, Count, spellPTR, CasterObject, Stacks };		
		if (DevMode && nID == Player.GetNetworkID() && Menu::Get<bool>("HealthPred.Menu.DrawBuffs")) {
			SdkUiConsoleWrite("Player Got Buff %s", Name);
		}
	}
	else {
		//Erase Buff from Target AIs Map
		if (BuffLookUp.count(nID) > 0) {
			BuffLookUp[nID].erase(buffName);
		}

		//Erase Spell struct if no other buff is using it
		if (BuffSpellLookUp.count(buffName) > 0) {
			auto buffsUsingSpell{ 0 };
			for (auto &[_, map] : BuffLookUp) {
				if (map.count(buffName) > 0) {
					++buffsUsingSpell;
				}
			}
			if (buffsUsingSpell == 0)
				BuffSpellLookUp.erase(buffName);
		}				
	}	
}


void __cdecl BuffManager::BuffChange(
	void * AI,
	unsigned char Type,
	float StartTime,
	float EndTime,
	const char * Name,
	void * CasterObject,
	unsigned int CasterID,
	int Stacks,
	bool HasCount,
	int Count,
	PSDK_SPELL Spell,
	void * UserData)
{
	UNREFERENCED_PARAMETER(CasterID);
	UNREFERENCED_PARAMETER(Spell);
	UNREFERENCED_PARAMETER(UserData);	

	if (Name == NULL)
		return;	

	std::string buffName{ Name };
	Common::ToLower(&buffName);
	unsigned int nID{};
	SdkGetObjectNetworkID(AI, &nID);
	if (!nID) { return; }

	//Update Buff to Target AIs Map
	auto &buff{ BuffLookUp[nID][buffName] };
	buff.Name = buffName;
	buff.Type = Type;
	buff.StartTime = StartTime;
	buff.EndTime = EndTime;
	buff.HasCount = HasCount;
	buff.Count = Count;
	buff.Caster = CasterObject;
	buff.Stacks = Stacks;	
}

bool __cdecl BuffManager::OnDelete(void * Object, unsigned int NetworkID, void * UserData) {
	UNREFERENCED_PARAMETER(UserData);

	if (Object && NetworkID) {
		//Erase Spell struct if no other buff is using it
		for (auto &[buffName, Instance] : BuffLookUp[NetworkID]) {			
			if (BuffSpellLookUp.count(buffName) > 0) {
				auto buffsUsingSpell{ 0 };
				for (auto &[_, map] : BuffLookUp) {
					if (map.count(buffName) > 0) {
						++buffsUsingSpell;
					}
				}
				if (buffsUsingSpell == 0)
					BuffSpellLookUp.erase(buffName);
			}
		}		

		BuffLookUp.erase(NetworkID);		
	}	
	return true;
}
bool __cdecl BuffManager::InitializeExistingBuffs(void * Object, unsigned int NetworkID, void * UserData) {
	UNREFERENCED_PARAMETER(NetworkID);
	UNREFERENCED_PARAMETER(UserData);

	if (!Object || !SDKSTATUS_SUCCESS(SdkIsObjectAI(Object)))
		return true;

	SdkEnumAIBuffs(Object,
		[](unsigned char Type, float StartTime, float EndTime, const char* Name, void* CasterObject, unsigned int CasterID, int Stacks, bool HasCount, int Count, PSDK_SPELL Spell, void* UserData)  -> bool {
			float CurrentTime;
			SdkGetGameTime(&CurrentTime);

			if (CurrentTime < EndTime && Stacks > 0) {
				BuffCreateDelete(UserData, true, Type, StartTime, EndTime, Name, CasterObject, CasterID, Stacks, HasCount, Count, Spell, NULL);
			}
			return true;
		},
		Object
	);

	return true;
}
