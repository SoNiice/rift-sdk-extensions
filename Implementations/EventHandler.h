#pragma once

#pragma warning(push, 0)
#pragma warning(disable: 4365)
#include <map>
#include <vector>
#include <queue>
#include <functional>
#pragma warning(pop)

#include "SDK Extensions.h"

struct DelayedAction_t {
	void* LambdaAddress = NULL;
	std::function<void()> Execute;
	unsigned int Time;
};

class EventHandler : public IEventHandler {
public:
	EventHandler();
	~EventHandler() {};

	#pragma region Interface
	void RegisterCallback(CallbackEnum t, void* func, void* UserData=NULL);
	void DelayedAction(std::function<void()> func, int delay);

	void FirePostAttack(AttackableUnit* Target);
	void FireUnkillable(AIMinionClient* Target);	
	#pragma endregion

	static void FirePreMove(bool* Process, Vector3* Position);
	static void FirePreAttack(bool* Process, AttackableUnit** Target);
	static void FirePreCast(bool* Process, PSDK_SPELL Spell, Vector3* Position, AttackableUnit** Target);
	static void FireInterruptible(AIHeroClient * Source, int Danger, float EndTime, bool CantMove, PSDK_SPELL_CAST Spell);
	static void FireGapcloser(AIHeroClient* Source, GapcloseData Data, PSDK_SPELL_CAST Spell);
	
	static unsigned long LastTick;
	static std::map<CallbackEnum, bool> IsRunning;

	#pragma region Containers
	static std::queue<DelayedAction_t> DelayedActionsQueue;
	static std::vector<DelayedAction_t> DelayedActions;
	static std::vector<RENDERSCENECALLBACK> TickCallbacks;		

	static std::vector<INTERRUPTIBLECALLBACK>	InterruptibleCallbacks;
	static std::vector<DASHCALLBACK>			DashCallbacks;
	static std::vector<GAPCLOSERCALLBACK>		GapcloserCallbacks;

	static std::vector<PREMOVECALLBACK  >    PreMoveCallbacks;
	static std::vector<PREATTACKCALLBACK>    PreAttackCallbacks;
	static std::vector<PRECASTCALLBACK  >    PreCastCallbacks;
	static std::vector<POSTATTACKCALLBACK>   PostAttackCallbacks;
	static std::vector<UNKILLABLECALLBACK>   UnkillableCallbacks;

	static std::map<unsigned int, bool> IsVisible;
	static std::vector<VISIONCALLBACK  > GainVisionCallbacks;
	static std::vector<VISIONCALLBACK  > LoseVisionCallbacks;
	#pragma endregion

	#pragma region Handlers
		static void TickHandler(void* UserData);
		static void DashHandler(void* AI, bool Move, bool Stop, void* UserData);	
		static void	VisionHandler(void* UserData);	
		static void __cdecl ModuleUnloadHandler(int Event, bool IsDependent, const char* ModuleName, void* ImageBase, size_t SizeOfImage, void* UserData);
	#pragma endregion
};





