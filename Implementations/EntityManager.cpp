#include "EntityManager.h"

#pragma region Maps
std::map<unsigned int, AIMinionClient> EntityManager::m_AzirSoldiers{};

std::map<unsigned int, AIMinionClient> EntityManager::m_EnemyWards{};
std::map<unsigned int, AIMinionClient> EntityManager::m_AllyWards{};
		 
std::map<unsigned int, AIMinionClient> EntityManager::m_EnemyMinions{};
std::map<unsigned int, AIMinionClient> EntityManager::m_AllyMinions{};
std::map<unsigned int, AIMinionClient> EntityManager::m_JungleMonsters{};
	 	 
std::map<unsigned int, AITurretClient> EntityManager::m_EnemyTurrets{};
std::map<unsigned int, AITurretClient> EntityManager::m_AllyTurrets{};
	 	
std::map<unsigned int, AIHeroClient> EntityManager::m_EnemyHeroes{};
std::map<unsigned int, AIHeroClient> EntityManager::m_AllyHeroes{};
		 
std::map<unsigned int, MissileClient> EntityManager::m_EnemyMissiles{};
std::map<unsigned int, MissileClient> EntityManager::m_AllyMissiles{};
		 
std::map<unsigned int, GameObject>		EntityManager::m_Particles{};
std::map<unsigned int, GameObject>		EntityManager::m_Shops{};
std::map<unsigned int, AttackableUnit>	EntityManager::m_Inhibitors{};
std::map<unsigned int, AttackableUnit>	EntityManager::m_Nexus{};		
std::map<unsigned int, GameObject*> EntityManager::m_ObjectPointers{};
#pragma endregion

bool EntityManager::CreateObj(void * Object, unsigned int NetworkID, void * UserData) {
	UNREFERENCED_PARAMETER(UserData);

	if (!Object || !NetworkID) { return true; }	
	int typeID = -1; CHECKRAWFAIL(SdkGetObjectTypeID(Object, &typeID));

	switch (typeID) {
		case ((int)GameObjectType::AIHeroClient):{
			AIHeroClient tmpHero(Object);
			(tmpHero.IsEnemy() ? m_EnemyHeroes[NetworkID] : m_AllyHeroes[NetworkID]) = tmpHero;
			m_ObjectPointers[NetworkID] = (tmpHero.IsEnemy() ? &m_EnemyHeroes[NetworkID] : &m_AllyHeroes[NetworkID]);			
			break; 
		}
		case ((int)GameObjectType::obj_AI_Minion):{
			AIMinionClient tmpMinion(Object);			

			if (tmpMinion.GetTeamID() == TEAM_TYPE_NEUTRAL) {
				m_JungleMonsters[NetworkID] = tmpMinion;
				m_ObjectPointers[NetworkID] = &m_JungleMonsters[NetworkID];
			}
			else if (tmpMinion.IsWard()) {
				(tmpMinion.IsEnemy() ? m_EnemyWards[NetworkID] : m_AllyWards[NetworkID]) = tmpMinion;
				m_ObjectPointers[NetworkID] = (tmpMinion.IsEnemy() ? &m_EnemyWards[NetworkID] : &m_AllyWards[NetworkID]);				
			}			
			else if (tmpMinion.GetType() == MINION_TYPE_LANE_MINION || tmpMinion.GetType() == MINION_TYPE_PET) {
				(tmpMinion.IsEnemy() ? m_EnemyMinions[NetworkID] : m_AllyMinions[NetworkID]) = tmpMinion;
				m_ObjectPointers[NetworkID] = (tmpMinion.IsEnemy() ? &m_EnemyMinions[NetworkID] : &m_AllyMinions[NetworkID]);				
			}
			
			if (tmpMinion.IsAlly() && strcmp(tmpMinion.GetCharName(), "AzirSoldier") == 0) {
				m_AzirSoldiers[NetworkID] = tmpMinion;
			}

			break;
		}
		case ((int)GameObjectType::obj_AI_Turret):{
			AITurretClient tmpTurret(Object);
			(tmpTurret.IsEnemy() ? m_EnemyTurrets[NetworkID] : m_AllyTurrets[NetworkID]) = tmpTurret;
			m_ObjectPointers[NetworkID] = (tmpTurret.IsEnemy() ? &m_EnemyTurrets[NetworkID] : &m_AllyTurrets[NetworkID]);			
			break;
		}
		case ((int)GameObjectType::MissileClient):{
			MissileClient tmpMissile(Object);
			(tmpMissile.IsEnemy() ? m_EnemyMissiles[NetworkID] : m_AllyMissiles[NetworkID]) = tmpMissile;
			m_ObjectPointers[NetworkID] = (tmpMissile.IsEnemy() ? &m_EnemyMissiles[NetworkID] : &m_AllyMissiles[NetworkID]);
			break;
		}
		case ((int)GameObjectType::obj_GeneralParticleEmitter):{
			GameObject tmpParticle(Object);
			m_Particles[NetworkID] = tmpParticle;
			m_ObjectPointers[NetworkID] = &m_Particles[NetworkID];
			break;
		}
		case ((int)GameObjectType::obj_Shop):{
			GameObject tmpShop(Object);
			m_Shops[NetworkID] = tmpShop;
			m_ObjectPointers[NetworkID] = &m_Shops[NetworkID];
			break;
		}
		case ((int)GameObjectType::obj_BarracksDampener):{
			AttackableUnit tmpBarrack(Object);
			m_Inhibitors[NetworkID] = tmpBarrack;
			m_ObjectPointers[NetworkID] = &m_Inhibitors[NetworkID];
			break;
		}
		case ((int)GameObjectType::obj_HQ):{
			AttackableUnit tmpHQ(Object);
			m_Nexus[NetworkID] = tmpHQ;
			m_ObjectPointers[NetworkID] = &m_Nexus[NetworkID];						
			break;
		}
	}
	return true;
}
bool EntityManager::DeleteObj(void * Object, unsigned int NetworkID, void * UserData) {
	UNREFERENCED_PARAMETER(UserData);			
	
	if (Object == NULL || !pSDK->EntityManager->IsValidObj(NetworkID)) {return true;}		
	m_ObjectPointers.erase(NetworkID);

	int typeID = -1; CHECKRAWFAIL(SdkGetObjectTypeID(Object, &typeID));
	GameObject tmpObject(Object);

	switch(typeID) {
		case ((int)GameObjectType::AIHeroClient):{
			(tmpObject.IsEnemy() ? m_EnemyHeroes : m_AllyHeroes).erase(NetworkID);			
			break;
		}	
		case ((int)GameObjectType::obj_AI_Minion):{
			AIMinionClient tmpMinion(Object);
			
			if (tmpMinion.GetTeamID() == TEAM_TYPE_NEUTRAL) {
				m_JungleMonsters.erase(NetworkID);
			}
			else if (tmpMinion.IsWard()) {
				(tmpMinion.IsEnemy() ? m_EnemyWards : m_AllyWards).erase(NetworkID);				
			} 			
			else if (tmpMinion.GetType() == MINION_TYPE_LANE_MINION || tmpMinion.GetType() == MINION_TYPE_PET) {
				(tmpMinion.IsEnemy() ? m_EnemyMinions : m_AllyMinions).erase(NetworkID);				
			}

			if (m_AzirSoldiers.find(NetworkID) != m_AzirSoldiers.end()) {
				m_AzirSoldiers.erase(NetworkID);
			}
			break;
		}
		case ((int)GameObjectType::obj_AI_Turret):{
			(tmpObject.IsEnemy() ? m_EnemyTurrets : m_AllyTurrets).erase(NetworkID);			
			break;
		}
		case ((int)GameObjectType::MissileClient):{
			(tmpObject.IsEnemy() ? m_EnemyMissiles : m_AllyMissiles).erase(NetworkID);			
			break;
		}
		case ((int)GameObjectType::obj_GeneralParticleEmitter):{
			m_Particles.erase(NetworkID);	break;
		}
		case ((int)GameObjectType::obj_Shop):{
			m_Shops.erase(NetworkID);		break;
		}
		case ((int)GameObjectType::obj_BarracksDampener):{
			m_Inhibitors.erase(NetworkID);	break;
		}
		case ((int)GameObjectType::obj_HQ):{
			m_Nexus.erase(NetworkID);		break;
		}	
	}

	//SdkUiConsoleWrite("Ending %s Callback", __FUNCTION__);
	return true;
}

std::map<unsigned int, void*> ObjectsOnGameEngine{};
bool __cdecl EntityManager::CheckObjects(void * Object, unsigned int NetworkID, void * UserData) {	
	if (!Object || !NetworkID) {
		SdkUiConsoleWrite("[error] Invalid Object found on SdkEnumGameObjects ptr: %p   netID: %d ", Object, NetworkID);
		return true;
	}

	bool IsObjectFromEngine{ UserData == NULL };
	if (IsObjectFromEngine)
		ObjectsOnGameEngine[NetworkID] = Object;

	unsigned int nID{ 0 };
	auto stateSDK{ SdkGetObjectNetworkID(Object, &nID) };
	if (stateSDK != SDKSTATUS_NO_ERROR || nID != NetworkID) {
		SdkUiConsoleWrite("[error] Object %d from %s is not valid on Core ", Object, IsObjectFromEngine ? "SdkEnumGameObjects" : "SDK-Extensions");
	}

	if (EntityManager::m_ObjectPointers.count(NetworkID) == 0) {
		SdkUiConsoleWrite("[error] Object %d from %s is not valid on SDK-Extensions ", Object, IsObjectFromEngine ? "SdkEnumGameObjects" : "SDK-Extensions");
	}

	if (IsObjectFromEngine && ObjectsOnGameEngine.count(NetworkID) == 0) {
		SdkUiConsoleWrite("[error] Object %d from SDK-Extensions is not valid on SdkEnumGameObjects", Object);
	}

	return true;
}

void __cdecl EntityManager::DrawMenu(void * UserData) {
	UNREFERENCED_PARAMETER(UserData);

	Menu::Button("Test Objects", []() {
		ObjectsOnGameEngine.clear();
		SdkEnumGameObjects(CheckObjects, NULL);

		for (auto &[netID, InstancePtr] : m_ObjectPointers) {
			auto rawPtr{ InstancePtr->PTR() };
			CheckObjects(rawPtr, netID, InstancePtr);
		}
	});
}

#pragma region Get Lists
template<class T>
std::map<unsigned int, T*> FilterListRange(std::map<unsigned int, T> &List, float Range = HUGE_VALF, Vector3* From = NULL) {	
	std::map<unsigned int, T*> Result;	
	auto checkRangeFrom = ((From == NULL) ? Player.GetPosition() : *From);
	for (auto & [nID, Object] : List) {
		Vector3 Pos{ Object.GetPosition() };
		if (Range < EPSILON || Range >= 25000.0f || checkRangeFrom.Distance(Pos) < Range) {
			Result[nID] = &Object;			
		}
	}
	return Result;
}

#define ENTITY_GET(NAME, TYPE, LIST) \
std::map<unsigned int, TYPE*>  EntityManager::Get##NAME(float Range, Vector3* From) { \
	return FilterListRange<TYPE>(LIST, Range, From); \
}

ENTITY_GET(AzirSoldiers, AIMinionClient, m_AzirSoldiers);

ENTITY_GET(EnemyHeroes   , AIHeroClient, m_EnemyHeroes);
ENTITY_GET(AllyHeroes    , AIHeroClient, m_AllyHeroes);

ENTITY_GET(EnemyMinions  , AIMinionClient, m_EnemyMinions);
ENTITY_GET(AllyMinions   , AIMinionClient, m_AllyMinions);
ENTITY_GET(JungleMonsters, AIMinionClient, m_JungleMonsters);

ENTITY_GET(EnemyTurrets  , AITurretClient, m_EnemyTurrets);
ENTITY_GET(AllyTurrets   , AITurretClient, m_AllyTurrets);

ENTITY_GET(EnemyMissiles  , MissileClient, m_EnemyMissiles);
ENTITY_GET(AllyMissiles   , MissileClient, m_AllyMissiles);

ENTITY_GET(EnemyWards  , AIMinionClient, m_EnemyWards);
ENTITY_GET(AllyWards   , AIMinionClient, m_AllyWards);

ENTITY_GET(Particles	, GameObject, m_Particles);
ENTITY_GET(Shops		, GameObject, m_Shops);

ENTITY_GET(Nexus		, AttackableUnit, m_Nexus);
ENTITY_GET(Inhibitors	, AttackableUnit, m_Inhibitors);
#pragma endregion

AITurretClient* EntityManager::GetNearestTurret(AIBaseClient* Obj, Vector3* Pos, int Team) {

	Vector3 tmpPos = (Pos != NULL) ? (*Pos) : Obj->GetPosition();
	AITurretClient* closest = NULL;
	float closestDist = HUGE_VAL;

	if (Team != Player.GetTeamID()) {
		for (auto &[_, Turret] : m_EnemyTurrets) {
			if (Turret.IsAlive()) {
				float curDist = Turret.GetPosition().Distance(tmpPos);
				if (curDist < closestDist) {
					closestDist = curDist;
					closest = &Turret;
				}
			}
		}
	}

	if (Team != (300 - Player.GetTeamID())) {
		for (auto &[_, Turret] : m_AllyTurrets) {
			if (Turret.IsAlive()) {
				float curDist = Turret.GetPosition().Distance(tmpPos);
				if (curDist < closestDist) {
					closestDist = curDist;
					closest = &Turret;
				}
			}
		}
	}

	return closest;
}

bool EntityManager::IsValidObj(void* Obj) {
	if (Obj == NULL) { return false; }

	unsigned int nID; CHECKRAWFAIL(SdkGetObjectNetworkID(Obj, &nID));
	return (nID != NULL && m_ObjectPointers.find(nID) != m_ObjectPointers.end());
}
bool EntityManager::IsValidObj(unsigned int nID) {
	return (nID != NULL && m_ObjectPointers.find(nID) != m_ObjectPointers.end());
}

GameObject* EntityManager::GetObjectFromPTR(void* Object) {
	if (!IsValidObj(Object)) {return NULL;}

	unsigned int nID; CHECKFAIL(SdkGetObjectNetworkID(Object, &nID));
	return m_ObjectPointers[nID];
}
GameObject* EntityManager::GetObjectFromID(unsigned int ID) {
	return IsValidObj(ID) ? m_ObjectPointers[ID] : NULL;
}

AIHeroClient EntityManager::GetLocalPlayer() {
	return AIHeroClient(g_LocalPlayer);
}
AIHeroClient LocalPlayer() {
	return pSDK->EntityManager->GetLocalPlayer();
}

EntityManager::EntityManager() {
	CHECKRAWFAIL(SdkGetLocalPlayer(&g_LocalPlayer));

	if (!g_LocalPlayer) {
		SdkUiConsoleWrite("Entity Manager Couldn't Retrieve Local Player");
		return;
	}	

	Player = AIHeroClient(g_LocalPlayer);
	pSDK->EventHandler->RegisterCallback(CallbackEnum::Tick, Game::UpdateLocalPlayer);

	SdkEnumGameObjects(CreateObj, NULL);
	pSDK->EventHandler->RegisterCallback(CallbackEnum::CreateObject, CreateObj);
	pSDK->EventHandler->RegisterCallback(CallbackEnum::DeleteObject, DeleteObj);

	if (DevMode) {
		pSDK->EventHandler->RegisterCallback(CallbackEnum::Overlay, DrawMenu);
	}

}
