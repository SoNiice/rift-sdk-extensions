#include "InterruptibleHandler.h"
#include "EventHandler.h"

InterruptibleSpellData InterruptibleHandler::_invalidData{};
InterruptibleSpellDataInst InterruptibleHandler::_invalidDataInst{};

std::map<std::string, std::vector<InterruptibleSpellData>> InterruptibleHandler::InterruptibleSpells{	
	{"Caitlyn", {
			{3, "CaitlynAceintheHole", SpellDangerLevel::VeryHigh, true},
	}
	},
	{ "FiddleSticks", {
			{1, "Drain", SpellDangerLevel::Medium, true},
			{3, "Crowstorm", SpellDangerLevel::VeryHigh, true},
		}
	},
	{ "Janna", {
			{3, "ReapTheWhirlwind", SpellDangerLevel::Medium, true},
		}
	},
	{ "Jhin", {
			{3, "JhinR", SpellDangerLevel::VeryHigh, true},
		}
	},
	{ "Karthus", {
			{3, "KarthusFallenOne", SpellDangerLevel::VeryHigh, true},
		}
	},
	{ "Katarina", {
			{3, "KatarinaR", SpellDangerLevel::VeryHigh, true},
		}
	},
	{ "Lucian", {
			{3, "LucianR", SpellDangerLevel::High, false},
		}
	},
	{ "Lux", {
			{3, "LuxMaliceCannon", SpellDangerLevel::VeryHigh, true},
		}
	},
	{ "Malzahar", {
			{3, "MalzaharR", SpellDangerLevel::VeryHigh, true},
		}
	},
	{ "MasterYi", {
			{1, "Meditate", SpellDangerLevel::Low, true},
		}
	},
	{ "MissFortune", {
			{3, "MissFortuneBulletTime", SpellDangerLevel::VeryHigh, true},
		}
	},
	{ "Neeko", {
			{3, "NeekoR", SpellDangerLevel::VeryHigh, true},
		}
	},
	{ "Nunu", {
			{3, "NunuR", SpellDangerLevel::VeryHigh, true},
		}
	},
	{ "Pantheon", {
			{2, "PantheonE", SpellDangerLevel::Low, true},
			{3, "PantheonRJump", SpellDangerLevel::VeryHigh, true},
		}
	},
	{ "Quinn", {
			{3, "QuinnR", SpellDangerLevel::High, true},
		}
	},
	{ "Shen", {
			{3, "ShenR", SpellDangerLevel::Low, true},
		}
	},
	{ "Sion", {
			{0, "SionQ", SpellDangerLevel::High, true},
		}
	},
	{ "TahmKench", {
			{3, "TahmKenchNewR", SpellDangerLevel::High, true},
		}
	},
	{ "TwistedFate", {
			{3, "Destiny", SpellDangerLevel::Medium, true},
		}
	},
	{ "Velkoz", {
			{3, "VelkozR", SpellDangerLevel::VeryHigh, true},
		}
	},
	{ "Vi", {
			{0, "ViQ", SpellDangerLevel::Medium, false},
		}
	},
	{ "Vladimir", {
			{2, "VladimirE", SpellDangerLevel::Low, false},
		}
	},
	{ "Warwick", {
			{3, "WarwickR", SpellDangerLevel::VeryHigh, true},
		}
	},
	{ "Xerath", {
			{0, "XerathArcanopulseChargeUp", SpellDangerLevel::Medium, false},
			{3, "XerathLocusOfPower2", SpellDangerLevel::High, true},
		}
	},
	{ "Zac", {
			{2, "ZacE", SpellDangerLevel::Medium, true},
		}
	},
	{ "Zilean", {
			{52, "Zilean_Passive", SpellDangerLevel::Low, true}, //Not Sure About the name
		}
	},	
};
std::vector<InterruptibleSpellData> InterruptibleHandler::CommonSpells {
	{66, "SummonerTeleport", SpellDangerLevel::Low, true},
	//{13, "", SpellDangerLevel::Low, true},
};

std::map<unsigned int, InterruptibleSpellData> InterruptibleHandler::ActiveSpells{};

InterruptibleHandler::InterruptibleHandler() {
	_invalidData = { SPELL_SLOT_MAX + 1, "", SpellDangerLevel::Low, false };
	_invalidDataInst = { NULL, 0.0f, {}, _invalidData};

	pSDK->EventHandler->RegisterCallback(CallbackEnum::Update, InterruptibleHandler::Tick);
	pSDK->EventHandler->RegisterCallback(CallbackEnum::SpellCastStart, InterruptibleHandler::OnProcessSpell);
}

bool InterruptibleHandler::IsCastingInterruptibleSpell(unsigned int nID, float delay) {
	if (delay < 0.1f) {
		return ActiveSpells.count(nID) > 0;
	}

	auto obj{ pSDK->EntityManager->GetObjectFromID(nID) };
	auto Hero{ obj ? obj->AsAIHeroClient() : NULL };
	if (!Hero || !Hero->IsAlive()) { return false; }

	auto activeSpell{ Hero->GetActiveSpell() };
	if (ActiveSpells.count(nID) == 0 || !activeSpell.Valid || (!Hero->IsCasting() && !activeSpell.IsCharging && !activeSpell.IsChanneling)) {
		return false;
	}
	return activeSpell.EndTime > (Game::Time() + delay);
}
InterruptibleSpellDataInst InterruptibleHandler::GetInterruptibleSpellDataInst(unsigned int nID) {
	auto obj{ pSDK->EntityManager->GetObjectFromID(nID) };
	auto Hero{ obj ? obj->AsAIHeroClient() : NULL };
	if (!Hero || !Hero->IsAlive()) { return _invalidDataInst; }

	auto activeSpell{ Hero->GetActiveSpell() };
	if (ActiveSpells.count(nID) == 0 || !activeSpell.Valid || (!Hero->IsCasting() && !activeSpell.IsCharging && !activeSpell.IsChanneling)) {
		return _invalidDataInst;
	}

	return { Hero, activeSpell.EndTime, activeSpell.SpellCast, ActiveSpells[nID] };
}

bool InterruptibleHandler::IsInterruptibleSpell(std::string championName, unsigned char slot) {
	return GetInterruptibleSpellData(championName, slot).IsValid();
}
bool InterruptibleHandler::IsInterruptibleSpell(std::string championName, std::string spellName) {
	return GetInterruptibleSpellData(championName, spellName).IsValid();
}
bool InterruptibleHandler::HasInterruptibleSpell(std::string championName) {
	return (InterruptibleSpells.count(championName) > 0 && !InterruptibleSpells[championName].empty());
}

InterruptibleSpellData InterruptibleHandler::GetInterruptibleSpellData(std::string championName, unsigned char slot) {
	if (InterruptibleSpells.count(championName) > 0) {
		for (auto &Data : InterruptibleSpells[championName]) {
			if (Data.Slot == slot) {
				return Data;
			}
		}
	}

	return _invalidData;
}
InterruptibleSpellData InterruptibleHandler::GetInterruptibleSpellData(std::string championName, std::string spellName) {
	if (InterruptibleSpells.count(championName) > 0) {
		for (auto &Data : InterruptibleSpells[championName]) {
			if (Data.Name == spellName) {				
				return Data;
			}
		}
	}

	return _invalidData;
}

std::vector<InterruptibleSpellData> InterruptibleHandler::GetInterruptibleSpellData(std::string championName) {
	std::vector<InterruptibleSpellData> res{};

	if (InterruptibleSpells.count(championName) > 0) {
		for (auto &Data : InterruptibleSpells[championName]) {
			res.push_back(Data);			
		}
	}

	return res;
}

void InterruptibleHandler::AddInterruptibleSpell(std::string championName, InterruptibleSpellData& data) {
	RemoveInterruptibleSpell(championName, data.Name);	
	InterruptibleSpells[championName].push_back(data);
}
void InterruptibleHandler::RemoveInterruptibleSpell(std::string championName, std::string spellName) {
	if (InterruptibleSpells.count(championName) > 0) {
		auto &spellList{ InterruptibleSpells[championName] };
		for (auto it = spellList.begin(); it != spellList.end(); ++it) {
			if (it->Name == spellName) {
				spellList.erase(it);
				return;
			}
		}
	}
}

void __cdecl InterruptibleHandler::Tick(void * UserData) {
	UNREFERENCED_PARAMETER(UserData);

	if (EventHandler::InterruptibleCallbacks.empty()) { return; }

	auto Heroes{ pSDK->EntityManager->GetEnemyHeroes() };
	auto Allies{ pSDK->EntityManager->GetAllyHeroes() };
	Heroes.insert(Allies.begin(), Allies.end());

	for (auto &[_, Hero] : Heroes) {
		auto nID{ Hero->GetNetworkID() };
		auto activeSpell{ Hero->GetActiveSpell() };

		if (ActiveSpells.count(nID) > 0) {
			if (!Hero->IsAlive() || !activeSpell.Valid || (!Hero->IsCasting() && !activeSpell.IsCharging && !activeSpell.IsChanneling)) {
				ActiveSpells.erase(nID);
			}
		}

		if (!activeSpell.Valid || ActiveSpells.count(nID) == 0) {
			continue;
		}

		auto &Data {ActiveSpells[nID]};
		EventHandler::FireInterruptible(Hero, (int)Data.DangerLevel, activeSpell.EndTime, Data.MovementInterrupts, &(activeSpell.SpellCast));
	}		
}
void __cdecl InterruptibleHandler::OnProcessSpell(void * AI, PSDK_SPELL_CAST SpellCast, void * UserData) {
	UNREFERENCED_PARAMETER(UserData);

	if (EventHandler::InterruptibleCallbacks.empty() || !AI || !SpellCast || !(SpellCast->Spell.ScriptName)) { return; }

	auto tmp{ pSDK->EntityManager->GetObjectFromPTR(AI) };
	auto Unit{ tmp ? tmp->AsAIHeroClient() : NULL };
	if (Unit == NULL || ActiveSpells.count(Unit->GetNetworkID()) > 0) {
		return;
	}

	std::string charName{ Unit->GetCharName() };
	if (pSDK->InterruptibleManager->HasInterruptibleSpell(charName)) {
		for (auto & Spell : InterruptibleSpells[charName]) {
			if (Spell.Slot == SpellCast->Spell.Slot) {				
				ActiveSpells[Unit->GetNetworkID()] = Spell;
				Tick(NULL);
				return;
			}
		}
	}

	std::string spellName{ SpellCast->Spell.ScriptName };
	for (auto & Spell : CommonSpells) {
		if (Spell.Name == spellName) {
			ActiveSpells[Unit->GetNetworkID()] = Spell;
			Tick(NULL);
			return;
		}
	}
}
