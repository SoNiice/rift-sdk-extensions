#pragma once

#include "SDK Extensions.h"

class InterruptibleHandler : public IInterruptible {
	static InterruptibleSpellData _invalidData;
	static InterruptibleSpellDataInst _invalidDataInst;
	
	static std::map<std::string, std::vector<InterruptibleSpellData>> InterruptibleSpells;
	static std::vector<InterruptibleSpellData> CommonSpells;

	static std::map<unsigned int, InterruptibleSpellData> ActiveSpells;

public:
	InterruptibleHandler();
	~InterruptibleHandler() {};

	bool IsCastingInterruptibleSpell(unsigned int nID, float delay = 0.0f) final;
	InterruptibleSpellDataInst GetInterruptibleSpellDataInst(unsigned int nID) final;

	bool IsInterruptibleSpell(std::string championName, unsigned char slot) final;
	bool IsInterruptibleSpell(std::string championName, std::string spellName) final;
	bool HasInterruptibleSpell(std::string championName) final;

	InterruptibleSpellData GetInterruptibleSpellData(std::string championName, unsigned char slot) final;
	InterruptibleSpellData GetInterruptibleSpellData(std::string championName, std::string spellName) final;
	std::vector<InterruptibleSpellData> GetInterruptibleSpellData(std::string championName) final;

	void AddInterruptibleSpell(std::string championName, InterruptibleSpellData& data) final;
	void RemoveInterruptibleSpell(std::string championName, std::string spellName) final;	

	static void	__cdecl	Tick(void* UserData);
	static void __cdecl OnProcessSpell(void* AI, PSDK_SPELL_CAST SpellCast, void* UserData);
};
