#include "EventHandler.h"

unsigned long EventHandler::LastTick;
std::map<CallbackEnum, bool> EventHandler::IsRunning;

std::queue<DelayedAction_t>  EventHandler::DelayedActionsQueue;
std::vector<DelayedAction_t> EventHandler::DelayedActions;

std::vector<RENDERSCENECALLBACK>		EventHandler::TickCallbacks;
	 
std::vector<PREMOVECALLBACK	  >			EventHandler::PreMoveCallbacks;
std::vector<PREATTACKCALLBACK >			EventHandler::PreAttackCallbacks;
std::vector<PRECASTCALLBACK   >			EventHandler::PreCastCallbacks;
std::vector<POSTATTACKCALLBACK>			EventHandler::PostAttackCallbacks;
std::vector<UNKILLABLECALLBACK>			EventHandler::UnkillableCallbacks;

std::map<unsigned int, bool> EventHandler::IsVisible;
std::vector<VISIONCALLBACK>				EventHandler::GainVisionCallbacks;
std::vector<VISIONCALLBACK>				EventHandler::LoseVisionCallbacks;

std::vector<INTERRUPTIBLECALLBACK>	EventHandler::InterruptibleCallbacks;
std::vector<DASHCALLBACK>			EventHandler::DashCallbacks;
std::vector<GAPCLOSERCALLBACK>		EventHandler::GapcloserCallbacks;

#define REGISTER_HANDLER(FUNC) \
if (IsRunning.count(t) == 0) { \
FUNC; \
IsRunning[t] = true; \
} 

template<class T>
void AddCustomCallback(std::vector<T>& CallbackList, T func) {
	for (T &FuncAlreadyAdded : CallbackList) {
		if (FuncAlreadyAdded == func) { return; }
	}
	CallbackList.push_back(func);
}
template<class T>
void RemoveCustomCallback(std::vector<T>& CallbackList, ULONG_PTR minAddr, ULONG_PTR maxAddr) {
	for (auto it = CallbackList.begin(); it != CallbackList.end();) {
		auto curAddr{ (ULONG_PTR)(*it) };
		if (curAddr > minAddr && curAddr < maxAddr) {
			it = CallbackList.erase(it);
		}
		else {
			++it;
		}
	}
}
void RemoveDelayedActions(ULONG_PTR minAddr, ULONG_PTR maxAddr) {
	auto &DelayedActions { EventHandler::DelayedActions };
	auto &DelayedActionsQueue{ EventHandler::DelayedActionsQueue };

	//Process All Queued Delayed Actions
	while (!DelayedActionsQueue.empty()) {
		DelayedActions.push_back(DelayedActionsQueue.front());
		DelayedActionsQueue.pop();
	}

	for (auto it = DelayedActions.begin(); it != DelayedActions.end();) {
		auto curAddr{ (ULONG_PTR)(it->LambdaAddress) };
		if (curAddr > minAddr && curAddr < maxAddr) {
			it = DelayedActions.erase(it);
		}
		else {
			++it;
		}
	}
}

void EventHandler::RegisterCallback(CallbackEnum t, void* func, void* UserData) {
	switch (t) {
	case (CallbackEnum::CreateObject):
		SdkRegisterCallback((GAMEOBJECTSCALLBACK)func, UserData, CALLBACK_TYPE_OBJECT_CREATE, CALLBACK_POSITION_BACK);		
		break;
	case (CallbackEnum::DeleteObject):
		SdkRegisterCallback((GAMEOBJECTSCALLBACK)func, UserData, (CALLBACK_TYPE_OBJECT_DELETE | 0x80000000), CALLBACK_POSITION_FRONT);
		break;
	case (CallbackEnum::NewPath):
		SdkRegisterCallback((ONAIMOVECALLBACK)func, UserData, CALLBACK_TYPE_AI_MOVE, CALLBACK_POSITION_BACK);
		break;
	case (CallbackEnum::Attack):
		SdkRegisterCallback((ONAIATTACKCALLBACK)func, UserData, CALLBACK_TYPE_AI_ATTACK, CALLBACK_POSITION_BACK);
		break;
	case (CallbackEnum::Update):
		SdkRegisterCallback((RENDERSCENECALLBACK)func, UserData, CALLBACK_TYPE_GAME_SCENE, CALLBACK_POSITION_BACK);
		break;
	case (CallbackEnum::Overlay):
		SdkRegisterCallback((RENDERSCENECALLBACK)func, UserData, CALLBACK_TYPE_OVERLAY_SCENE, CALLBACK_POSITION_BACK);
		break;
	case (CallbackEnum::SpellCastEnd):
		SdkRegisterCallback((ONAICASTATTACKCALLBACK)func, UserData, CALLBACK_TYPE_AI_CAST_ATTACK, CALLBACK_POSITION_BACK);
		break;
	case (CallbackEnum::SpellCastStart):
		SdkRegisterCallback((ONAICASTATTACKCALLBACK)func, UserData, CALLBACK_TYPE_AI_PROCESS_SPELL, CALLBACK_POSITION_BACK);
		break;
	case (CallbackEnum::Recall):
		SdkRegisterCallback((ONUNITRECALLCALLBACK)func, UserData, CALLBACK_TYPE_UNIT_RECALL, CALLBACK_POSITION_BACK);
		break;
	case (CallbackEnum::InventoryUpdate):
		SdkRegisterCallback((ONPLAYERSHOPCALLBACK)func, UserData, CALLBACK_TYPE_PLAYER_SHOP, CALLBACK_POSITION_BACK);
		break;
	case (CallbackEnum::BuffUpdate):
		SdkRegisterCallback((ONAIBUFFUPDATECALLBACK)func, UserData, CALLBACK_TYPE_AI_BUFF_UPDATE, CALLBACK_POSITION_BACK);
		break;
	case (CallbackEnum::BuffCreateAndDelete):
		SdkRegisterCallback((ONAIBUFFCREATEDELETECALLBACK)func, UserData, CALLBACK_TYPE_AI_BUFF_CREATE_DELETE, CALLBACK_POSITION_BACK);
		break;
	case (CallbackEnum::ModuleLoadAndUnload):
		SdkRegisterCallback((ONATTACHDETACHCALLBACK)func, UserData, CALLBACK_TYPE_MODULE_ATTACH_DETACH, CALLBACK_POSITION_BACK);
		break;


	case (CallbackEnum::Tick):
		LastTick = 0UL;
		REGISTER_HANDLER(SdkRegisterGameScene(TickHandler, UserData));
		AddCustomCallback(TickCallbacks, (RENDERSCENECALLBACK)func);
		break;
	case (CallbackEnum::PreAttack):
		AddCustomCallback(PreAttackCallbacks, (PREATTACKCALLBACK)func);
		break;
	case (CallbackEnum::PreMove):
		AddCustomCallback(PreMoveCallbacks, (PREMOVECALLBACK)func);		
		break;
	case (CallbackEnum::PreCast):
		AddCustomCallback(PreCastCallbacks, (PRECASTCALLBACK)func);		
		break;
	case (CallbackEnum::PostAttack):
		AddCustomCallback(PostAttackCallbacks, (POSTATTACKCALLBACK)func);		
		break;
	case (CallbackEnum::GainVision):
		AddCustomCallback(GainVisionCallbacks, (VISIONCALLBACK)func);		
		REGISTER_HANDLER(RegisterCallback(CallbackEnum::Tick, VisionHandler));
		break;
	case (CallbackEnum::LoseVision):
		t = CallbackEnum::GainVision;
		AddCustomCallback(LoseVisionCallbacks, (VISIONCALLBACK)func);			
		REGISTER_HANDLER(RegisterCallback(CallbackEnum::Tick, VisionHandler));
		break;
	case (CallbackEnum::UnkillableMinion):
		AddCustomCallback(UnkillableCallbacks, (UNKILLABLECALLBACK)func);		
		break;
	case (CallbackEnum::Interruptible):
		AddCustomCallback(InterruptibleCallbacks, (INTERRUPTIBLECALLBACK)func);		
		break;
	case (CallbackEnum::Dash):
		AddCustomCallback(DashCallbacks, (DASHCALLBACK)func);		
		REGISTER_HANDLER(RegisterCallback(CallbackEnum::Tick, DashHandler));
		break;
	case (CallbackEnum::GapCloser):
		AddCustomCallback(GapcloserCallbacks, (GAPCLOSERCALLBACK)func);		
		break;
	}
}
void EventHandler::DelayedAction(std::function<void()> func, int delay) {
	if (delay <= 0) {
		func();		
	}
	else {
		DelayedActionsQueue.push({ Common::GetLambdaAddress(func), func, Common::GetTickCount() + (unsigned int)delay});
		constexpr auto t{ CallbackEnum::Tick };
		REGISTER_HANDLER(SdkRegisterGameScene(TickHandler, NULL));
	}	
}

EventHandler::EventHandler() {
	SdkRegisterOnModuleAttachDetach(EventHandler::ModuleUnloadHandler, NULL);
}

void __cdecl EventHandler::ModuleUnloadHandler(int Event, bool IsDependent, const char * ModuleName, void * ImageBase, size_t SizeOfImage, void * UserData) {
	UNREFERENCED_PARAMETER(UserData);
		
	if (IsDependent && ImageBase != NULL && Event == SDK_EVENT_UNLOAD_MODULE) {
		ULONG_PTR minAddr{ (ULONG_PTR)ImageBase };
		ULONG_PTR maxAddr{ minAddr + SizeOfImage};

		RemoveDelayedActions(minAddr, maxAddr);
		RemoveCustomCallback(TickCallbacks,			 minAddr, maxAddr);
		RemoveCustomCallback(InterruptibleCallbacks, minAddr, maxAddr);
		RemoveCustomCallback(DashCallbacks,			 minAddr, maxAddr);
		RemoveCustomCallback(GapcloserCallbacks,	 minAddr, maxAddr);
		RemoveCustomCallback(PreMoveCallbacks,		 minAddr, maxAddr);
		RemoveCustomCallback(PreAttackCallbacks,	 minAddr, maxAddr);
		RemoveCustomCallback(PreCastCallbacks,		 minAddr, maxAddr);
		RemoveCustomCallback(PostAttackCallbacks,	 minAddr, maxAddr);
		RemoveCustomCallback(UnkillableCallbacks,	 minAddr, maxAddr);
		RemoveCustomCallback(GainVisionCallbacks,	 minAddr, maxAddr);
		RemoveCustomCallback(LoseVisionCallbacks,	 minAddr, maxAddr);

		if (ModuleName)
			SdkUiConsoleWrite("[SDK Extensions] All Custom Callbacks From [%s] Have Been Unloaded", ModuleName);
	}	
}

#define FIRE_CALLBACKS(CONTAINER, ...) \
if (!CONTAINER.empty()) {\
    for (auto it = CONTAINER.begin(); it != CONTAINER.end();) { \
        auto func = *it; \
        auto result {SdkGetLoadedModule(func, NULL, NULL, NULL)}; \
        if (SDKSTATUS_SUCCESS(result)) { \
            func(__VA_ARGS__); \
            ++it; \
        } \
        else { it = CONTAINER.erase(it); }\
    }\
}


void EventHandler::TickHandler(void* UserData) {
	unsigned long Tick = Common::GetTickCount();
	if ((Tick - EventHandler::LastTick) > (1000 / 30)) {
		EventHandler::LastTick = Tick;
		FIRE_CALLBACKS(TickCallbacks, UserData);		
	}	

	while (!DelayedActionsQueue.empty()) {
		DelayedActions.push_back(DelayedActionsQueue.front());
		DelayedActionsQueue.pop();
	}
	
	if (!DelayedActions.empty()) {
		for (auto it = DelayedActions.begin(); it != DelayedActions.end();) {			
			
			bool ModuleLoaded{ SDKSTATUS_SUCCESS(SdkGetLoadedModule(it->LambdaAddress, NULL, NULL, NULL)) };
			if (ModuleLoaded && Tick >= it->Time) {
				it->Execute();		
				it = DelayedActions.erase(it);
			}					
			else {
				++it;
			}			
		}
	}
};
void EventHandler::DashHandler(void* AI, bool Move, bool Stop, void* UserData) {
	UNREFERENCED_PARAMETER(Move);
	UNREFERENCED_PARAMETER(Stop);
	UNREFERENCED_PARAMETER(UserData);

	GameObject* obj{ pSDK->EntityManager->GetObjectFromPTR(AI) };
	AIHeroClient* Hero{ (obj != NULL) ? obj->AsAIHeroClient() : NULL };
	if (Hero && Hero->IsDashing()) {
		auto nav{ Hero->NavInfo() };
		FIRE_CALLBACKS(DashCallbacks, Hero, &(nav.StartPos), &(nav.EndPos) , Common::GetTickCount(), (unsigned int) (nav.EndPos.Distance(nav.StartPos) / nav.DashSpeed * 1000.0f), nav.DashSpeed);			
	}
}
void EventHandler::VisionHandler(void* UserData) {
	UNREFERENCED_PARAMETER(UserData);

	auto Enemies{ pSDK->EntityManager->GetEnemyHeroes() };
	for (auto &[_, Enemy] : Enemies) {
		auto nID = Enemy->GetNetworkID();		
		if (EventHandler::IsVisible.count(nID) == 0) {
			EventHandler::IsVisible[nID] = Enemy->IsVisible();
		}
		else if (EventHandler::IsVisible[nID] && !Enemy->IsVisible()) {
			EventHandler::IsVisible[nID] = false;
			FIRE_CALLBACKS(LoseVisionCallbacks, Enemy);			
		}
		else if (!EventHandler::IsVisible[nID] && Enemy->IsVisible()) {
			EventHandler::IsVisible[nID] = true;
			FIRE_CALLBACKS(GainVisionCallbacks, Enemy);									
		}		
	}	
}

void EventHandler::FirePreMove(bool* Process, Vector3* Position) {
	FIRE_CALLBACKS(PreMoveCallbacks, Process, Position);	
}
void EventHandler::FirePreAttack(bool* Process, AttackableUnit** Target) {
	FIRE_CALLBACKS(PreAttackCallbacks, Process, Target);	
}
void EventHandler::FirePreCast(bool* Process, PSDK_SPELL Spell, Vector3* Position, AttackableUnit** Target) {
	FIRE_CALLBACKS(PreCastCallbacks, Process, Spell, Position, Target);	
}
void EventHandler::FirePostAttack(AttackableUnit* Target) {
	FIRE_CALLBACKS(PostAttackCallbacks, Target);	
}
void EventHandler::FireUnkillable(AIMinionClient * Target){
	FIRE_CALLBACKS(UnkillableCallbacks, Target);	
}
void EventHandler::FireInterruptible(AIHeroClient* Source, int Danger, float EndTime, bool CantMove, PSDK_SPELL_CAST Spell) {
	FIRE_CALLBACKS(InterruptibleCallbacks, Source, Danger, EndTime, CantMove, Spell);
}
void EventHandler::FireGapcloser(AIHeroClient* Source, GapcloseData Data, PSDK_SPELL_CAST Spell) {
	FIRE_CALLBACKS(GapcloserCallbacks, Source, Data, Spell);
}
