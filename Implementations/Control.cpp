#include "Control.h"
#include "EventHandler.h"

unsigned int Control::LastAction;
constexpr bool DONT_CHECK_SPELLS{ false };

#define RETURN_RESULT(BOOL) \
bool result = BOOL; \
if (result) { LastAction = GetTickCount(); } \
return result;

#define CHECK_HUMANIZER(TICKS) \
if (GetTickCount() - LastAction < TICKS) {	return false;}

bool Control::Move(){
	CHECK_HUMANIZER(20);
	Vector3 Pos{ Renderer::MousePos() };	
	bool Process = true;
	EventHandler::FirePreMove(&Process, &Pos);
	RETURN_RESULT(Process && Pos != NULL && Pos.IsValid() && SDKSTATUS_SUCCESS(SdkMoveLocalPlayer(&Pos, false)) );	
}

bool Control::Move(Vector3* Pos, bool Pet) {
	CHECK_HUMANIZER(20);
	bool Process = true;
	EventHandler::FirePreMove(&Process, Pos);
	RETURN_RESULT(Process && Pos != NULL && Pos->IsValid() && SDKSTATUS_SUCCESS(SdkMoveLocalPlayer(Pos, Pet)) );	
}

bool Control::Attack() {
	CHECK_HUMANIZER(20);
	Vector3 Pos{ Renderer::MousePos() };
	RETURN_RESULT(Pos.IsValid() && SDKSTATUS_SUCCESS(SdkAttackMoveLocalPlayer(&Pos)) );		
}

bool Control::Attack(AttackableUnit * OriginalTarget, bool Pet) {
	CHECK_HUMANIZER(20);
	if (OriginalTarget == NULL || !OriginalTarget->IsValidTarget(DONT_CHECK_SPELLS) || OriginalTarget->IsAlly()) { return false; }
	
	auto Target  = OriginalTarget;
	bool Process = true;
	EventHandler::FirePreAttack(&Process, &Target);	

	if (Target == NULL || !Target->IsValidTarget(DONT_CHECK_SPELLS) || Target->IsAlly()) {
		Target = OriginalTarget;
	}

	RETURN_RESULT(Process && SDKSTATUS_SUCCESS(SdkAttackTargetLocalPlayer(Target->PTR(), Pet)));
}

bool Control::CastSpell(unsigned char Slot, AttackableUnit * OriginalTarget, bool Release) {
	if (OriginalTarget == NULL || !OriginalTarget->IsValidTarget()) { return false; }

	SDK_SPELL Spell = Player.GetSpell(Slot);	
	auto Target  = OriginalTarget;
	bool Process = true;
	EventHandler::FirePreCast(&Process, &Spell, NULL, &Target);

	if (Target == NULL || !Target->IsValidTarget()) {
		Target = OriginalTarget;
	}

	unsigned char State = (unsigned char)((Release) ? SPELL_CAST_COMPLETE : SPELL_CAST_START);
	RETURN_RESULT(Process && Target != NULL && pSDK->EntityManager->IsValidObj(Target->GetNetworkID()) && Target->IsAlive() && SDKSTATUS_SUCCESS(SdkCastSpellLocalPlayer(Target->PTR(), NULL, Slot, State))  );
}

bool Control::CastSpell(unsigned char Slot, Vector3 * Pos, bool Release) {
	SDK_SPELL Spell = Player.GetSpell(Slot);
	bool Process = true;
	EventHandler::FirePreCast(&Process, &Spell, Pos, NULL);

	unsigned char State = (unsigned char)((Release) ? SPELL_CAST_COMPLETE : SPELL_CAST_START);
	RETURN_RESULT(Process && Pos != NULL && Pos->IsValid() && SDKSTATUS_SUCCESS(SdkCastSpellLocalPlayer(NULL, Pos, Spell.Slot, State)) );
}

bool Control::CastSpell(unsigned char Slot, Vector3 * startPos, Vector3 * endPos, bool Release) {
	UNREFERENCED_PARAMETER(Slot);
	UNREFERENCED_PARAMETER(startPos);
	UNREFERENCED_PARAMETER(endPos);
	UNREFERENCED_PARAMETER(Release);	

	bool result = false;
	if (result) { LastAction = GetTickCount(); };
	return result;	
}

bool Control::CastSpell(unsigned char Slot, bool Release) {
	CHECK_HUMANIZER(Game::Ping() / 2);	

	SDK_SPELL Spell = Player.GetSpell(Slot);
	bool Process = true;
	EventHandler::FirePreCast(&Process, &Spell, NULL, NULL);	
	
	auto pos{ Player.GetPosition() };
	unsigned char State = (unsigned char)((Release) ? SPELL_CAST_COMPLETE : SPELL_CAST_START);
	RETURN_RESULT(Process && SDKSTATUS_SUCCESS(SdkCastSpellLocalPlayer(NULL, &pos, Slot, State)) );
}

bool Control::MoveMouse(Vector3* Pos) {
	return Pos != NULL && !SDKSTATUS_SUCCESS(SdkMoveMouse(Pos));
}
