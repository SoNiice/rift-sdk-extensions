#pragma once

#include "SDK Extensions.h"

class GapcloseTracker : public IGapclose {
	static GapcloseData _invalidData;
	static GapcloseDataInst _invalidDataInst;

	static std::map<std::string, std::vector<GapcloseData>> GapcloseDatabase;
	static std::map<unsigned int, GapcloseDataInst> ActiveGapclose;
public:

	GapcloseTracker();
	~GapcloseTracker() {};

	bool IsBlinking(unsigned int netID) final;
	bool IsDashing (unsigned int netID) final;
	GapcloseDataInst GetGapcloseDataInst(unsigned int nID) final;
	
	bool HasGapclose(std::string championName) final;
	GapcloseData GetGapcloseData(std::string championName, std::string spellName) final;
	std::vector<GapcloseData> GetGapcloseData(std::string championName) final;

	void AddGapclose(std::string championName, GapcloseData& data) final;
	void RemoveGapclose(std::string championName, std::string spellName) final;

	static void	__cdecl	Update(void* UserData);
	static void	__cdecl	Tick(void* UserData);
	static void __cdecl OnProcessSpell(void* AI, PSDK_SPELL_CAST SpellCast, void* UserData);
	static void __cdecl OnNewPath(void * AI, bool Move, bool Stop, void * UserData);
};

	