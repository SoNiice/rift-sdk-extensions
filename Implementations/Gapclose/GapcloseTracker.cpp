#include "GapcloseTracker.h"
#include "EventHandler.h"


GapcloseData GapcloseTracker::_invalidData{};
GapcloseDataInst GapcloseTracker::_invalidDataInst{};
std::map<unsigned int, GapcloseDataInst> GapcloseTracker::ActiveGapclose{};

#pragma region Debug Variables
unsigned int lastCastSpell{ 0u };
Vector3 lastCastPos{};
bool spellWasCast{ false };
unsigned int lastMove{ 0u };
unsigned int lastStop{ 0u };
#pragma endregion

GapcloseData MakeBlink(std::string spellName, float range, float speed, float windUp, float windDown, bool canVaryInLength = true, bool IsTargetted = false) {
	return { spellName, true, IsTargetted, canVaryInLength, range, speed, windUp, windDown };
}
GapcloseData MakeDash(std::string spellName, float range, float speed, float windUp, float windDown, bool canVaryInLength = true, bool IsTargetted = false) {
	return { spellName, false, IsTargetted, canVaryInLength, range, speed, windUp, windDown };
}

#pragma warning(push)
#pragma warning (disable: 4868)
std::map<std::string, std::vector<GapcloseData>> GapcloseTracker::GapcloseDatabase{
	{"All", {
			(MakeBlink("SummonerFlash", 420.0f, HUGE_VALF, 0.0f, 0.0f)),
		}
	},
	{ "Ahri",    { MakeDash("AhriTumble", 460.0f, 1570.0f, 0.1f, 0.1f) } },
	{ "Akali",   { MakeDash("AkaliEb", HUGE_VALF, 1740.0f, 0.035f, 0.06f, true, true), MakeDash("AkaliR" , 690.0f, 1550.0f, 0.0f, 0.06f, true, true)}},
	{ "Alistar", { MakeDash("Headbutt", 510.0f, 1550.0f, 0.0f, 0.1f) } },
	{ "Amumu",   { MakeDash("BandageToss", 1000.0f, 1500.0f, 0.0f, 0.1f, true, true) } },
	{ "Azir",    { MakeDash("AzirEWrapper", 2000.0f, 1700.0f, 0.05f, 0.1f, true, true) } },
	{ "Braum",   { MakeDash("BraumW", 650.0f, 1530.0f, 0.0f, 0.2f, true, true) } },
	{ "Caitlyn", { MakeDash("CaitlynEntrapment", 375.0f, 1000.0f, 0.2f, 0.2f, false) } },
	{ "Camille", { 
			MakeDash("CamilleE", 400.0f, 1400.0f, 0.0f, 0.2f, true, true),
			MakeDash("CamilleEDash2", 800.0f, 1400.0f, 0.0f, 0.1f),
			MakeDash("CamilleR", 475.0f, 900.0f, 0.0f, 0.1f, true, true),
		} 
	},
	{ "Corki", {MakeDash("CarpetBomb", 600.0f, 1400.0f, 0.0f, 0.1f), MakeDash("CarpetBombMega", 1800.0f, 1500.0f, 0.0f, 0.1f)}},
	{ "Diana", { MakeDash("DianaTeleport", 750.0f, 2350.0f, 0.0f, 0.15f, true, true) } },
	{ "Ekko", {
			MakeDash("EkkoE", 350.0f, 1150.0f, 0.0f, 0.2f),
			MakeBlink("EkkoEAttack", 800.0f, HUGE_VALF, 0.1f, 0.3f, true, true),
			MakeBlink("EkkoR", HUGE_VALF, HUGE_VALF, 0.5f, 0.1f),
		}
	},
	{ "Elise",        { MakeBlink("EliseSpiderEDescent", 750.0f, HUGE_VALF, 0.0f, 0.5f, true, true) } },
	{ "Evelynn",      { MakeBlink("EvelynnR", 700.0f, HUGE_VALF, 0.9f, 0.2f, false) } },
	{ "Ezreal",       { MakeBlink("EzrealE", 475.0f, HUGE_VALF, 0.3f, 0.2f) }},
	{ "Fiddlesticks", { MakeBlink("Crowstorm", 800.0f, HUGE_VALF, 1.5f, 0.2f)}},
	{ "Fiora",		  { MakeDash ("FioraQ", 400.0f, 1100.0f, 0.0f, 0.1f)}},
	{ "Fizz",		  { MakeDash ("FizzQ", 550.0f, 1400.0f, 0.0f, 0.0f, true, true)}},
	{ "Galio",		  { MakeDash ("GalioE", 560.0f, 2300.0f, 0.6f, 0.0f, false)}},
	{ "Garen",		  { MakeDash ("GarenQAttack", 150.0f, 2300.0f, 0.0f, 0.1f, true, true)}},
	{ "Gnar",		  { MakeDash ("GnarE", 430.0f, 900.0f, 0.0f, 0.0f)}},
	{ "Gragas",		  { MakeDash ("GragasE", 600.0f, 910.0f, 0.0f, 0.1f, false)}},
	{ "Graves",		  { MakeDash ("GravesMove", 375.0f, 1140.0f, 0.0f, 0.1f)}},	
	{ "Irelia",		  { MakeDash ("IreliaQ", 680.0f, 1835.0f, 0.0f, 0.0f, true, true)}},
	{ "JarvanIV",	  { MakeDash ("JarvanIVDragonStrike", 800.0f, 2400.0f, 0.5f, 0.0f, true, true)}},
	{ "Jax",		  { MakeDash ("JaxLeapStrike", 600.0f, 1680.0f, 0.0f, 0.0f, true, true)}},	
	{ "Jayce",		  { MakeDash ("JayceToTheSkies", 420.0f, 960.0f, 0.0f, 0.0f, true, true)}},
	{ "Kassadin",	  { MakeBlink("RiftWalk", 500.0f, HUGE_VALF, 0.3f, 0.0f)}},
	{ "Katarina",	  {	MakeBlink("KatarinaE", 725.0f, HUGE_VALF, 0.0f, 0.0f),	MakeBlink("KatarinaEDagger", 775.0f, HUGE_VALF, 0.0f, 0.0f)}}, 
	{ "Kayn",		  { MakeDash ("KaynQ", 350.0f, 1020.0f, 0.0f, 0.35f)}},
	{ "Khazix",		  { MakeDash ("KhazixE", 650.0f, 1250.0f, 0.0f, 0.1f)}},
	{ "Kindred",	  { MakeDash ("KindredQ", 300.0f, 835.0f, 0.0f, 0.1f)}},
	{ "Kled",		  { MakeDash ("KledE", 540.0f, 945.0f, 0.0f, 0.1f), MakeDash("KledE2", 540.0f, 1100.0f, 0.0f, 0.0f, false, true) }},
	{ "Leblanc",	  { MakeDash ("LeblancW", 600.0f, 1450.0f, 0.0f, 0.0f), MakeDash("LeblancRW", 600.0f, 1450.0f, 0.0f, 0.0f) }},
	{ "LeeSin",		  { MakeDash ("BlindMonkQTwo", 1500.0f, 2000.0f, 0.0f, 0.0f, true, true)}},
	{ "Leona",		  { MakeDash ("LeonaZenithBlade", 875.0f, 3000.0f, 0.8f, 0.2f, true, true)}},
	{ "Lucian",		  { MakeDash ("LucianE", 350.0f, 1350.0f, 0.0f, 0.1f)}},	
	{ "Malphite",	  { MakeDash ("UFSlash", 1000.0f, 1800.0f, 0.0f, 0.0f)}},
	{ "Maokai",		  { MakeDash ("MaokaiW", HUGE_VALF, 1300.0f, 0.0f, 0.1f, true, true)}},
	{ "MasterYi",	  { MakeBlink("AlphaStrike", 600.0f, HUGE_VALF, 0.0f, 0.5f, true, true)}},
	{ "Nidalee",	  { MakeDash ("Pounce", 300.0f, 950.0f, 0.0f, 0.0f)}},
	{ "Ornn",		  { MakeDash ("OrnnE", 700.0f, 1600.0f, 0.4f, 0.4f, false)}},
	{ "Pantheon",	  { MakeDash ("PantheonW", 350.0f, 1080.0f, 0.0f, 0.1f, true, true)}},
	{ "Poppy",		  { MakeDash ("PoppyE", 475.0f, 1800.0f, 0.0f, 0.0f, true, true)}},
	{ "Quinn",		  { MakeDash ("QuinnE", 675.0f, 2500.0f, 0.0f, 0.0f, true, true)}},
	{ "Rakan",		  { MakeDash ("RakanW", 600.0f, 1500.0f, 0.0f, 0.5f, true, true), MakeDash("RakanE", 1000.0f, 2000.0f, 0.0f, 0.0f, true, true)}},
	{ "RekSai",		  { 
			MakeDash ("RekSaiEBurrowed", 900.0f, 750.0f, 0.0f, 0.0f, false), 
			MakeDash ("RekSaiTunnelTime", 1000.0f, 900.0f, 0.0f, 0.0f, false),
			MakeBlink("ReksaiR", 1500.0f, HUGE_VALF, 1.5f, 0.5f, true, true)
		}
	},
	{ "Renekton",	  { MakeDash ("RenektonSliceAndDice", 450.0f, 1100.0f, 0.0f, 0.0f, false), MakeDash("RenektonDice", 450.0f, 1100.0f, 0.0f, 0.0f, false)}},
	{ "Riven",		  { MakeDash ("RivenFeint", 325.0f, 1200.0f, 0.0f, 0.0f, false), MakeDash("RivenTriCleave", 260.0f, 780.0f, 0.0f, 0.0f, false)}},
	{ "Sejuani",	  { MakeDash ("SejuaniQ", 650.0f, 1000.0f, 0.0f, 0.1f, false)}},
	{ "Shaco",		  { MakeBlink("Deceive", 400.0f, HUGE_VALF, 0.15f, 0.15f)}},
	{ "Shen",		  { MakeDash ("ShenE", 600.0f, 1600.0f, 0.0f, 0.1f)}},
	{ "Shyvana",	  { MakeDash ("ShyvanaTransformLeap", 850.0f, 1100.0f, 0.35f, 0.1f)}},
	{ "Talon",		  { MakeDash ("TalonQDashAttack", 450.0f, 1400.0f, 0.0f, 0.0f, true, true), MakeDash("TalonE", 1000.0f, 900.0f, 0.0f, 0.0f, true, true)}},
	{ "Thresh",		  { MakeDash ("ThreshQLeap", 1100.0f, 900.0f, 0.0f, 0.0f, true, true)}},
	{ "Tristana",	  { MakeDash ("TristanaW", 900.0f, 1100.0f, 0.25f, 0.05f)}},
	{ "Tryndamere",	  { MakeDash ("TryndamereE", 645.0f, 900.0f, 0.0f, 0.0f)}},
	{ "TwistedFate",  { MakeBlink("Gate", 5500.0f, HUGE_VALF, 1.6f, 0.0f)}},
	{ "Urgot",		  { MakeDash ("UrgotE", 475.0f, 1530.0f, 0.5f, 0.2f)}},
	{ "Vayne",		  { MakeDash ("VayneTumble", 300.0f, 830.0f, 0.0f, 0.0f)}},
	{ "Wukong",		  { MakeDash("MonkeyKingNimbus", 550.0f, 1400.0f, 0.0f, 0.0f, true, true)}},
	{ "XinZhao",	  { MakeDash("XinZhaoE", 600.0f, 3000.0f, 0.0f, 0.2f, true, true)}},
	{ "Yasuo",	      { MakeDash("YasuoEDash", 475.0f, 1040.0f, 0.0f, 0.2f, false)}},
	{ "Zac",	      { MakeDash("ZacE", 1800.0f, 1100.0f, 0.2f, 0.0f, true, true), MakeDash("ZacR", 1000.0f, 820.0f, 0.2f, 0.0f, true, true)}},
	{ "Zed",	      { 
			MakeBlink("ZedW2", 1950.0f, HUGE_VALF, 0.04f, 0.0f, true, true), 
			MakeBlink("ZedR" , 625.0f, HUGE_VALF, 0.0f, 0.3f, true, true),
			MakeBlink("ZedR2", HUGE_VALF, HUGE_VALF, 0.0f, 0.0f, true, true),
		}
	},

};
#pragma warning(pop)

GapcloseTracker::GapcloseTracker() {
	pSDK->EventHandler->RegisterCallback(CallbackEnum::Update, GapcloseTracker::Tick);
	pSDK->EventHandler->RegisterCallback(CallbackEnum::SpellCastStart, GapcloseTracker::OnProcessSpell);

	_invalidData = { "", false, false, false, 0.0f, 0.0f, 0.0f, 0.0f };
	_invalidDataInst = { NULL, 0.0f, {}, _invalidData };

	if (DevMode) {
		pSDK->EventHandler->RegisterCallback(CallbackEnum::NewPath, GapcloseTracker::OnNewPath);
		pSDK->EventHandler->RegisterCallback(CallbackEnum::Update, GapcloseTracker::Update);
	}
}

bool GapcloseTracker::IsBlinking(unsigned int netID) {
	return pSDK->GapcloserManager->GetGapcloseDataInst(netID).Data.IsBlink;
}
bool GapcloseTracker::IsDashing(unsigned int netID) {
	return pSDK->GapcloserManager->GetGapcloseDataInst(netID).Data.IsValid();
}

bool GapcloseTracker::HasGapclose(std::string championName) {
	return GapcloseDatabase.count(championName) > 0;
}
GapcloseDataInst GapcloseTracker::GetGapcloseDataInst(unsigned int nID) {
	if (ActiveGapclose.count(nID) == 0) {
		return _invalidDataInst;
	}
	return ActiveGapclose[nID];
}
GapcloseData GapcloseTracker::GetGapcloseData(std::string championName, std::string spellName) {
	if (GapcloseDatabase.count(championName) > 0) {
		std::vector<GapcloseData>& spellList{ GapcloseDatabase[championName] };
		for (auto &Spell : spellList) {
			if (Spell.SpellName == spellName) {
				return Spell;
			}
		}
	}

	std::vector<GapcloseData> spellList{ GapcloseDatabase["All"] };
	for (auto &Spell : spellList) {
		if (Spell.SpellName == spellName) {
			return Spell;
		}
	}
	return _invalidData;
}
std::vector<GapcloseData> GapcloseTracker::GetGapcloseData(std::string championName) {
	std::vector<GapcloseData> res{};
	if (GapcloseDatabase.count(championName) > 0) {
		std::vector<GapcloseData>& spellList{ GapcloseDatabase[championName] };
		for (auto &Spell : spellList) {
			res.push_back(Spell);
		}
	}
	return res;
}

void __cdecl GapcloseTracker::Tick(void * UserData) {
	UNREFERENCED_PARAMETER(UserData);

	//if (EventHandler::GapcloserCallbacks.empty()) { return; }

	auto curTime{ Game::Time() };
	auto Heroes{ pSDK->EntityManager->GetEnemyHeroes() };
	auto Allies{ pSDK->EntityManager->GetAllyHeroes() };
	Heroes.insert(Allies.begin(), Allies.end());

	for (auto &[_, Hero] : Heroes) {
		auto nID{ Hero->GetNetworkID() };

		if (ActiveGapclose.count(nID) > 0 && ActiveGapclose[nID].EndTime < curTime) {
			ActiveGapclose.erase(nID);
		}

		if (ActiveGapclose.count(nID) == 0) {			
			continue;
		}

		/*
		To Add:
			Rengar Jumps, Gnar Double Jump, IllaoiW, ViQ, WarwickQ and R
		
		*/

		auto &DataInst{ ActiveGapclose[nID] };
		auto targ{ pSDK->EntityManager->GetObjectFromPTR(DataInst.Spell.TargetObject) };
		if (targ) {
			auto curPos{ targ->GetPosition() };
			if (curPos.Distance(DataInst.Spell.EndPosition) > 5.0f) {
				auto delta{ curPos.Distance(DataInst.Spell.StartPosition) - DataInst.Spell.EndPosition.Distance(DataInst.Spell.StartPosition) };
				DataInst.EndTime += delta * DataInst.Data.Speed;
				DataInst.Spell.EndPosition = curPos;

				if (DataInst.Data.SpellName == "QuinnE") {
					DataInst.Spell.EndPosition = curPos.Extended(DataInst.Spell.StartPosition, 525.0f);
				}
			}			
		}
		else if (DataInst.Data.IsTargetted){
			if (!DataInst.Data.IsBlink) {
				if (!Hero->IsDashing()) {
					continue;
				}
				DataInst.Spell.EndPosition = Hero->NavInfo().EndPos;
			}
		}

		EventHandler::FireGapcloser(Hero, DataInst.Data, &(DataInst.Spell));
	}	
}
void __cdecl GapcloseTracker::OnProcessSpell(void * AI, PSDK_SPELL_CAST SpellCast, void * UserData) {
	UNREFERENCED_PARAMETER(UserData);

	if (!AI || !SpellCast || !SpellCast->Spell.ScriptName) { return; }	

	//if (DevMode && AI == Player.PTR()) {
	//	spellWasCast = true;
	//	lastCastSpell = Common::GetTickCount();
	//	lastCastPos = SpellCast->StartPosition;		
	//	SdkUiConsoleWrite("%s Casted %s at %d Player %p  target %p", Player.GetCharName(), SpellCast->Spell.ScriptName, lastCastSpell, Player.PTR(), SpellCast->TargetObject);
	//}

	auto tmp{ pSDK->EntityManager->GetObjectFromPTR(AI) };
	auto Sender{ tmp ? tmp->AsAIHeroClient() : NULL };
	if (!Sender) { return; }

	std::string senderName{ Sender->GetCharName() };
	std::string spellName{ SpellCast->Spell.ScriptName };

	auto gapCloser{ pSDK->GapcloserManager->GetGapcloseData(senderName, spellName) };
	if (!gapCloser.IsValid()) {
		return;;
	}

	auto &spellStart{ SpellCast->StartPosition };
	auto &spellEnd{ SpellCast->EndPosition };

	auto endPos = spellEnd;
	auto tmpTarg{ SpellCast->TargetObject ? pSDK->EntityManager->GetObjectFromPTR(SpellCast->TargetObject) : NULL };
	auto target{ tmpTarg ? tmpTarg->AsAIBaseClient() : NULL };

	if (gapCloser.CanVaryInLength == false || spellStart.Distance(endPos) > gapCloser.MaxRange) {
		endPos = spellStart.Extended(spellEnd, gapCloser.MaxRange);
	}

	if (gapCloser.SpellName == "AzirEWrapper") {
		auto soldiers{ pSDK->EntityManager->GetAzirSoldiers(1100.0f, &spellStart) };
		if (soldiers.empty()) { return; }

		auto closestSoldier{ std::min_element(soldiers.begin(), soldiers.end(), [&spellEnd](const std::pair<unsigned int, AIMinionClient*>& a, const std::pair<unsigned int, AIMinionClient*>& b) {
			return a.second->Distance(spellEnd) < b.second->Distance(spellEnd);
		})->second };

		endPos = closestSoldier->GetServerPosition();
		target = closestSoldier;		
	}
	else if (gapCloser.SpellName == "CaitlynEntrapment") {
		endPos = Sender->GetPosition().Extended(spellEnd, -375.0f);
	}
	else if (gapCloser.SpellName == "EvelynnR") {
		endPos = Sender->GetPosition().Extended(spellEnd, -700.0f);
	}
	else if (gapCloser.SpellName == "EkkoR") {
		bool bEnemyParticle{ Sender->IsEnemy() };
		auto particles{ pSDK->EntityManager->GetParticles() };
		if (particles.empty()) { return; }

		for (auto &[_, particle] : particles) {
			if (!(bEnemyParticle ^ particle->IsEnemy())) {
				if (strstr(particle->GetName(), "_R_TrailEnd")) {
					endPos = particle->GetPosition();
					break;
				}
			}			
		}		
	}
	else if (gapCloser.SpellName == "BlindMonkQTwo") {
		std::map<unsigned int, AIBaseClient*> nearby{};

		auto nearby1{ pSDK->EntityManager->GetJungleMonsters(1500.0f, &spellStart) };
		nearby.insert(nearby1.begin(), nearby1.end());
		auto nearby2{ Sender->IsEnemy() ? pSDK->EntityManager->GetAllyMinions(1500.0f, &spellStart) : pSDK->EntityManager->GetEnemyMinions(1500.0f, &spellStart) };
		nearby.insert(nearby2.begin(), nearby2.end());
		auto nearby3{ Sender->IsEnemy() ? pSDK->EntityManager->GetAllyHeroes(1500.0f, &spellStart) : pSDK->EntityManager->GetEnemyHeroes(1500.0f, &spellStart) };
		nearby.insert(nearby3.begin(), nearby3.end());		
		if (nearby.empty()) { return; }

		for (auto &[_, Base] : nearby) {
			if (Base->IsAlive() && Base->IsVisible() && Base->IsTargetable() && (Base->HasBuff("BlindMonkQOne") || Base->HasBuff("BlindMonkQOneChaos"))) {
				endPos = Base->GetServerPosition();
				target = Base;
				break;
			}
		}
	}
	else if (gapCloser.SpellName == "JarvanIVDragonStrike") {
		constexpr float JARVAN_Q_WIDTH{ 70.0f };

		auto nearby{ Sender->IsEnemy() ? pSDK->EntityManager->GetEnemyMinions(1000.0f, &spellStart) : pSDK->EntityManager->GetAllyMinions(1000.0f, &spellStart) };
		if (nearby.empty()) { return; }

		auto start2D{ spellStart.To2D() };
		auto end2D{ spellEnd.To2D() };

		for (auto &[_, minion] : nearby) {
			auto name{ minion->GetCharName() };
			if (name && strcmp(name, "JarvanIVStandard") == 0) {
				auto pos{ minion->GetPosition().To2D() };
				if (pos.Distance(start2D, end2D, true) < (JARVAN_Q_WIDTH + minion->GetBoundingRadius())) {
					endPos = minion->GetPosition();
					target = minion;
					break;
				}
			}			
		}
		if (!target) { return; }
	}
	else if (gapCloser.SpellName == "PoppyE") {
		if (!target) { return; }
		endPos = target->GetPosition().Extended(spellStart, -300.0f);
	}	
	else if (gapCloser.SpellName == "ThreshQLeap") {
		constexpr float THRESH_Q_WIDTH{ 70.0f };

		std::map<unsigned int, AIBaseClient*> nearby{};

		auto nearby1{ pSDK->EntityManager->GetJungleMonsters(1500.0f, &spellStart) };
		nearby.insert(nearby1.begin(), nearby1.end());
		auto nearby2{ Sender->IsEnemy() ? pSDK->EntityManager->GetAllyMinions(1500.0f, &spellStart) : pSDK->EntityManager->GetEnemyMinions(1500.0f, &spellStart) };
		nearby.insert(nearby2.begin(), nearby2.end());
		auto nearby3{ Sender->IsEnemy() ? pSDK->EntityManager->GetAllyHeroes(1500.0f, &spellStart) : pSDK->EntityManager->GetEnemyHeroes(1500.0f, &spellStart) };
		nearby.insert(nearby3.begin(), nearby3.end());

		if (nearby.empty()) { return; }

		for (auto &[_, minion] : nearby) {
			if (minion->IsAlive() && minion->IsVisible() && minion->IsTargetable() && minion->HasBuff("ThreshQ")) {
				auto pos{ minion->GetPosition().To2D() };
				endPos = minion->GetServerPosition();
				target = minion;
				break;
			}
		}
	}
	else if (gapCloser.SpellName == "RivenTriCleave") {
		gapCloser.IsTargetted = target ? true : false;
	}
	else if (gapCloser.SpellName == "YasuoEDash") {
		if (!target) { return; }
		auto curPos{ target->GetPosition() };

		gapCloser.Speed = 776.0f + 0.8f * Sender->GetMovementSpeed();
		endPos = spellStart.Extended(curPos, 475.0f);
	}
	else if (gapCloser.SpellName == "ZedW2") {
		auto minions{ Sender->IsEnemy() ? pSDK->EntityManager->GetEnemyMinions(1950.0f, &spellStart) : pSDK->EntityManager->GetAllyMinions(1950.0f, &spellStart) };
		if (minions.empty()) { return; }
	
		for (auto &[_, minion] : minions) {
			if (strcmp(minion->GetCharName(), "Shadow") == 0) {
				endPos = minion->GetPosition();
				target = minion;
				break;
			}
		}
	}
	else if (gapCloser.SpellName == "ZedR2") {
		auto minions{ Sender->IsEnemy() ? pSDK->EntityManager->GetEnemyMinions() : pSDK->EntityManager->GetAllyMinions() };
		if (minions.empty()) { return; }

		for (auto &[_, minion] : minions) {
			if (strcmp(minion->GetCharName(), "Shadow") == 0) {
				endPos = minion->GetPosition();
				target = minion;
				break;
			}
		}
	}

	if (target && gapCloser.IsTargetted) {
		endPos = target->GetServerPosition();
	}

	auto spellCast{ *SpellCast };
	spellCast.EndPosition = endPos;
	spellCast.TargetObject = target ? target->PTR() : NULL;

	auto endTime{ Game::Time() + gapCloser.WindUp + gapCloser.WindDown};
	if (!gapCloser.IsBlink) {
		endTime += spellCast.StartPosition.Distance(spellCast.EndPosition) / gapCloser.Speed;

		if (gapCloser.SpellName == "QuinnE") {
			endTime += spellCast.StartPosition.Distance(spellCast.EndPosition) / 825.0f;
			spellCast.EndPosition = endPos.Extended(spellStart, 525.0f);
		}
	}		

	ActiveGapclose[Sender->GetNetworkID()] = {Sender, endTime, spellCast, gapCloser};
	Tick(NULL);
}

void __cdecl GapcloseTracker::Update(void * UserData) {
	UNREFERENCED_PARAMETER(UserData);

	//if (spellWasCast) {
	//	if (Player.GetPosition().Distance(lastCastPos) > 5.0f) {
	//		SdkUiConsoleWrite("Started Moving After %d", Common::GetTickCount() - lastCastSpell);
	//		spellWasCast = false;
	//	}			
	//}
}
void __cdecl GapcloseTracker::OnNewPath(void * AI, bool Move, bool Stop, void * UserData) {
	UNREFERENCED_PARAMETER(AI);
	UNREFERENCED_PARAMETER(Move);
	UNREFERENCED_PARAMETER(Stop);
	UNREFERENCED_PARAMETER(UserData);

	//if (AI == Player.PTR()) {
	//	auto time{ Common::GetTickCount() };
	//	auto timeSinceCast{ Common::GetTickCount() - lastCastSpell};
	//	auto distSinceCast{ Player.Distance(lastCastPos) };
	//
	//	if (Move)
	//		lastMove = time;
	//	if (Stop)
	//		lastStop = time;
	//	
	//	SdkUiConsoleWrite("Player NewPath: Move %d Stop %d  Tick %d TicksSinceLastSpell %d DistanceSinceLastSpell %f  Speed %f or %f   windup %d  winddown %d", Move, Stop, time, timeSinceCast, distSinceCast, (distSinceCast *1000.0f)/(timeSinceCast), Player.NavInfo().DashSpeed, time-lastMove, time-lastStop);
	//}	
}

void GapcloseTracker::AddGapclose(std::string championName, GapcloseData& data) {
	RemoveGapclose(championName, data.SpellName);
	GapcloseDatabase[championName].push_back(data);
}
void GapcloseTracker::RemoveGapclose(std::string championName, std::string spellName) {
	if (GapcloseDatabase.count(championName) > 0) {
		auto &spellList{ GapcloseDatabase[championName] };
		for (auto it = spellList.begin(); it != spellList.end(); ++it) {
			if (it->SpellName == spellName) {
				spellList.erase(it);
				return;
			}
		}
	}
}

