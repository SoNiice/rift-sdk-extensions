// dllmain.cpp : Defines the entry point for the DLL application.
#include "stdafx.h"
#include "SDK Extensions.h"

#include "EventHandler.h"
#include "EntityManager.h"
#include "Control.h"
#include "DamageLib.h"
#include "HealthPrediction.h"
#include "BuffManager/BuffManager.h"
#include "Prediction/Collision.h"
#include "Prediction/Prediction.h"
#include "Interruptible/InterruptibleHandler.h"
#include "Gapclose/GapcloseTracker.h"

PLUGIN_SETUP("Rift SDK Extensions", OnLoad);

bool __cdecl LibraryCallback(const char * ImportName, unsigned int Version, void * Data, size_t * SizeOfData, void * UserData) {
	UNREFERENCED_PARAMETER(UserData);

	bool IsSDK = strcmp(ImportName, LIBRARY_IMPORT_SDK) == 0;

	if (IsSDK && Version != LIBRARY_VERSION_SDK) {
		SdkUiConsoleWrite("[SDK Extensions] Incompatible version of SDK Extensions.h. Latest Version: %u (Current: %u).\n", LIBRARY_VERSION_SDK, Version);
		return false;
	}

	// IsBadWritePtr checks that the SizeOfData memory location is writable (4 bytes).
	if (!Data || !SizeOfData || IsBadWritePtr(SizeOfData, sizeof(SizeOfData))) {
		SdkUiConsoleWrite("[SDK Extensions] Wrong Arguments.\n");
		return false;
	}

	if (IsSDK && pSDK) {
		BAD_BUFFER_CHECK(Data, *SizeOfData, sizeof(pSDK));

		std::shared_ptr<SDK>* SDK_Interface = (std::shared_ptr<SDK>*)Data;
		*SDK_Interface = pSDK;
		*SizeOfData = sizeof(*SDK_Interface);
	}
	else {
		SdkUiConsoleWrite("[SDK Extensions] Import '%s' Not Found", ImportName);
		return false;
	}

	// All's good!
	return true;
}

void __cdecl OnLoad(void* UserData) {
	UNREFERENCED_PARAMETER(UserData);
	CheckLocalPlayer();
	
	DevMode = false;
	pSDK = std::make_shared<SDK>();

	pSDK->EventHandler			= std::make_shared<EventHandler>();
	pSDK->EntityManager			= std::make_shared<EntityManager>();
	pSDK->Control				= std::make_shared<Control>();
	pSDK->DamageLib				= std::make_shared<DamageLib>();
	pSDK->HealthPred			= std::make_shared<HealthPred>();
	pSDK->Collision				= std::make_shared<Collision>();
	pSDK->Prediction			= std::make_shared<Prediction>();
	pSDK->BuffManager			= std::make_shared<BuffManager>();
	pSDK->InterruptibleManager  = std::make_shared<InterruptibleHandler>();
	pSDK->GapcloserManager		= std::make_shared<GapcloseTracker>();	
	
	SdkRegisterLibrary(LibraryCallback, NULL);	
	Game::PrintChat(R"(<font color="#832232">Rift SDK Extensions Loaded.</font>)");
}